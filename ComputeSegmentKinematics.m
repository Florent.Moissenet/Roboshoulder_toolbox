% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/Roboshoulder_Toolbox
% Reference    : To be defined
% Date         : March 2020
% -------------------------------------------------------------------------
% Description  : To be defined
% -------------------------------------------------------------------------
% Dependencies : To be defined
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function Trial = ComputeSegmentKinematics(Trial)

% Define ICS (robot coordinate system)
ICS = [1 0 0; 0 1 0; 0 0 1];

% Recompute segments position and orientation (raw)
for i = 1:size(Trial.Robot.Joint(1).Angle.raw,1)
    for j = 1:size(Trial.Robot.Segment,2)
        if j > 1 && j < size(Trial.Robot.Segment,2)
            Trial.Robot.Segment(j).T.raw(i,:,:) = permute(permute(...
                                                  Trial.Robot.Segment(j-1).T.raw(i,:,:),[2,3,1]) * ...
                                                  Trial.Robot.Segment(j).Geometry.M * ...
                                                  [cos(Trial.Robot.Joint(j-1).Angle.raw(i,:)) -sin(Trial.Robot.Joint(j-1).Angle.raw(i,:)) 0 0; ...
                                                   sin(Trial.Robot.Joint(j-1).Angle.raw(i,:))  cos(Trial.Robot.Joint(j-1).Angle.raw(i,:)) 0 0; ...
                                                                                            0                                           0 1 0; ...
                                                                                            0                                           0 0 1],[3,1,2]);
        elseif j == 1
            Trial.Robot.Segment(j).T.raw(i,:,:) = permute(Trial.Robot.Segment(j).Geometry.M,[3,1,2]);
        elseif j == size(Trial.Robot.Segment,2)
            Trial.Robot.Segment(j).T.raw(i,:,:) = permute(permute(Trial.Robot.Segment(j-1).T.raw(i,:,:),[2,3,1]) * ...
                                                                  Trial.Robot.Segment(j).Geometry.M,[3,1,2]);
        end
        Trial.Robot.Segment(j).Position.raw(i,:,:)      = Trial.Robot.Segment(j).T.raw(i,1:3,4);
        Trial.Robot.Segment(j).OrientationXY.raw(i,:,:) = atan2(norm(cross(permute(Trial.Robot.Segment(j).T.raw(i,1:3,1),[2,3,1]),ICS(:,1))), ...
                                                                dot(permute(Trial.Robot.Segment(j).T.raw(i,1:3,1),[2,3,1]),ICS(:,1)));
        Trial.Robot.Segment(j).OrientationXY.raw(i,:,:) = atan2(norm(cross(permute(Trial.Robot.Segment(j).T.raw(i,1:3,3),[2,3,1]),ICS(:,3))), ...
                                                                dot(permute(Trial.Robot.Segment(j).T.raw(i,1:3,3),[2,3,1]),ICS(:,3)));
    end
end

% Recompute segments position and orientation (cycle)
for icycle = 1:3
    for i = 1:101
        for j = 1:size(Trial.Robot.Segment,2)
            if j > 1 && j < size(Trial.Robot.Segment,2)
                Trial.Robot.Segment(j).T.cycle(i,:,:,icycle) = permute(permute(Trial.Robot.Segment(j-1).T.cycle(i,:,:,icycle),[2,3,1]) * ...
                                                                               Trial.Robot.Segment(j).Geometry.M * ...
                                                                               [cos(Trial.Robot.Joint(j-1).Angle.cycle(i,:,icycle)) -sin(Trial.Robot.Joint(j-1).Angle.cycle(i,:,icycle)) 0 0; ...
                                                                                sin(Trial.Robot.Joint(j-1).Angle.cycle(i,:,icycle))  cos(Trial.Robot.Joint(j-1).Angle.cycle(i,:,icycle)) 0 0; ...
                                                                                                                                  0                                                    0 1 0; ...
                                                                                                                                  0                                                    0 0 1],[3,1,2]);
            elseif j == 1
                Trial.Robot.Segment(j).T.cycle(i,:,:,icycle) = permute(Trial.Robot.Segment(j).Geometry.M,[3,1,2]);
            elseif j == size(Trial.Robot.Segment,2)
                Trial.Robot.Segment(j).T.cycle(i,:,:,icycle) = permute(permute(Trial.Robot.Segment(j-1).T.cycle(i,:,:,icycle),[2,3,1]) * ...
                                                                               Trial.Robot.Segment(j).Geometry.M,[3,1,2]);
            end            
            Trial.Robot.Segment(j).Position.cycle(i,:,:,icycle)      = Trial.Robot.Segment(j).T.cycle(i,1:3,4,icycle);
            Trial.Robot.Segment(j).OrientationXY.cycle(i,:,:,icycle) = atan2(norm(cross(permute(Trial.Robot.Segment(j).T.cycle(i,1:3,1,icycle),[2,3,1,4]),ICS(:,1))), ...
                                                                             dot(permute(Trial.Robot.Segment(j).T.cycle(i,1:3,1,icycle),[2,3,1,4]),ICS(:,1)));
            Trial.Robot.Segment(j).OrientationXY.cycle(i,:,:,icycle) = atan2(norm(cross(permute(Trial.Robot.Segment(j).T.cycle(i,1:3,3,icycle),[2,3,1,4]),ICS(:,3))), ...
                                                                             dot(permute(Trial.Robot.Segment(j).T.cycle(i,1:3,3,icycle),[2,3,1,4]),ICS(:,3)));
        end
    end
end