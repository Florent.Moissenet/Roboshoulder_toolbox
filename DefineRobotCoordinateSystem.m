clearvars;
close all;
% clc;
cd("C:\Users\moissene\Documents\Switchdrive\Projets\B-LAB\Projets\2021-2022 - ROBOWRIST\ATHLANI_PROSUP\Data\Running_test_20210608\");

calibrationFiles = {'staticRobot02.c3d'};

for icalibration = 1:size(calibrationFiles,2)

	% Load marker trajectories stored in C3D files
	% ---------------------------------------------------------------------
	btkFile     = btkReadAcquisition(calibrationFiles{icalibration});
	Marker      = btkGetMarkers(btkFile);
	markerNames = fieldnames(Marker); % Marker names in the marker trajectories
    dMarker     = 14.0; % Marker diameter
    
    % Keep only the mean position of the markers
	% ---------------------------------------------------------------------
    for imarker = 1:size(markerNames,1)
        Marker.(markerNames{imarker}) = mean(Marker.(markerNames{imarker}),1);
    end
    
	% Set virtual markers based on geometric assumptions
	% ---------------------------------------------------------------------
    Xend2 = [1 0 0];
    Yend2 = [0 1 0];
    Zend2 = [0 0 1];
	Oend2 = [0.0 0.0 0.0]; % Based on technical documentation (mm)
    Marker2.BAS01 = Oend2 ...
                    + 340/2*Xend2 ... % Defined on CAO documentation fromm HEPIA (mm)
                    - 220/2*Yend2 ... % Defined on CAO documentation fromm HEPIA (mm)
                    - dMarker/2*Zend2 ...
                    + 1*Zend2; % Depth of the marker base placement on the flange plate (mm)
    Marker2.BAS02 = Oend2 ...
                    - 340/2*Xend2 ... % Defined on CAO documentation fromm HEPIA (mm)
                    - 220/2*Yend2 ... % Defined on CAO documentation fromm HEPIA (mm)
                    - dMarker/2*Zend2 ...
                    + 1*Zend2; % Depth of the marker base placement on the flange plate (mm)
    Marker2.BAS03 = Oend2 ...
                    - 340/2*Xend2 ... % Defined on CAO documentation fromm HEPIA (mm)
                    + 220/2*Yend2 ... % Defined on CAO documentation fromm HEPIA (mm)
                    - dMarker/2*Zend2 ...
                    + 1*Zend2; % Depth of the marker base placement on the flange plate (mm)
    Marker2.BAS04 = Oend2 ...
                    + 340/2*Xend2 ... % Defined on CAO documentation fromm HEPIA (mm)
                    + 220/2*Yend2 ... % Defined on CAO documentation fromm HEPIA (mm)
                    - dMarker/2*Zend2 ...
                    + 1*Zend2; % Depth of the marker base placement on the flange plate (mm)
% 
% 	Calibration(icalibration).Tend  = Tend;
% 	Calibration(icalibration).Tend2 = Tend2;

end

% Define translation and rotation by matrix inversion
[R,d,rms]=soder([Marker.BAS01;Marker.BAS02;Marker.BAS03;Marker.BAS04],[Marker2.BAS01;Marker2.BAS02;Marker2.BAS03;Marker2.BAS04])

% figure;
% hold on;
% axis equal;
% plot3(Marker.BAS01(1),Marker.BAS01(2),Marker.BAS01(3),'Marker','o','Color','green');
% plot3(Marker.BAS02(1),Marker.BAS02(2),Marker.BAS02(3),'Marker','o','Color','red');
% plot3(Marker.BAS03(1),Marker.BAS03(2),Marker.BAS03(3),'Marker','o','Color','red');
% plot3(Marker.BAS04(1),Marker.BAS04(2),Marker.BAS04(3),'Marker','o','Color','red');
% plot3(Marker2.BAS01(1),Marker2.BAS01(2),Marker2.BAS01(3),'Marker','x','Color','green');
% plot3(Marker2.BAS02(1),Marker2.BAS02(2),Marker2.BAS02(3),'Marker','x','Color','blue');
% plot3(Marker2.BAS03(1),Marker2.BAS03(2),Marker2.BAS03(3),'Marker','x','Color','blue');
% plot3(Marker2.BAS04(1),Marker2.BAS04(2),Marker2.BAS04(3),'Marker','x','Color','blue');
% Test1 = (R*Marker.BAS01'+d)';
% Test2 = (R*Marker.BAS02'+d)';
% Test3 = (R*Marker.BAS03'+d)';
% Test4 = (R*Marker.BAS04'+d)';
% plot3(Test1(1),Test1(2),Test1(3),'Marker','.','Color','green');
% plot3(Test2(1),Test2(2),Test2(3),'Marker','.','Color','red');
% plot3(Test3(1),Test3(2),Test3(3),'Marker','.','Color','red');
% plot3(Test4(1),Test4(2),Test4(3),'Marker','.','Color','red');


for icalibration = 1:size(calibrationFiles,2)

	% Load marker trajectories stored in C3D files
	% ---------------------------------------------------------------------
	btkFile     = btkReadAcquisition(calibrationFiles{icalibration});
	Marker      = btkGetMarkers(btkFile);
	markerNames = fieldnames(Marker); % Marker names in the marker trajectories
    dMarker     = 14.0; % Marker diameter
    
    % Keep only the mean position of the markers
	% ---------------------------------------------------------------------
    for imarker = 1:size(markerNames,1)
        Marker.(markerNames{imarker}) = mean(Marker.(markerNames{imarker}),1);
    end
        
%     % Correct marker position
% 	% ---------------------------------------------------------------------
%     for imarker = 1:size(markerNames,1)
%         Marker.(markerNames{imarker}) = (R*Marker.(markerNames{imarker})'+d)';
%     end
    
	% Set virtual markers based on geometric assumptions
	% ---------------------------------------------------------------------
    Xend2 = [0 0 -1];
    Yend2 = [0 1 0];
    Zend2 = [1 0 0];
	Oend2 = [820.0 0.0 360.0]; % Based on technical documentation (mm)
    Marker2.END01 = Oend2 ...
                    + 81.5/2*(-Xend2) ... % Defined on CAO documentation fromm HEPIA (mm)
                    - dMarker/2*Zend2 ...
                    + 1*Zend2 ... % Depth of the marker base placement on the flange plate (mm)
                    + 152*Zend2; % Distance between top of the flange to centre of the iiwa_link_6 (mm)
    Marker2.END02 = Oend2 ...
                    + 81.5/2*(-Yend2) ... % Defined on CAO documentation fromm HEPIA (mm)
                    - dMarker/2*Zend2 ...
                    + 1*Zend2 ... % Depth of the marker base placement on the flange plate (mm)
                    + 152*Zend2; % Distance between top of the flange to centre of the iiwa_link_6 (mm)
    Marker2.END03 = Oend2 ...
                    + 81.5/2*Xend2 ... % Defined on CAO documentation fromm HEPIA (mm)
                    - dMarker/2*Zend2 ...
                    + 1*Zend2 ... % Depth of the marker base placement on the flange plate (mm)
                    + 152*Zend2; % Distance between top of the flange to centre of the iiwa_link_6 (mm)
    Marker2.END04 = Oend2 ...
                    + 81.5/2*Yend2 ... % Defined on CAO documentation fromm HEPIA (mm)
                    - dMarker/2*Zend2 ...
                    + 1*Zend2 ... % Depth of the marker base placement on the flange plate (mm)
                    + 152*Zend2; % Distance between top of the flange to centre of the iiwa_link_6 (mm)
end

% Define translation and rotation by matrix inversion
[R2,d2,rms]=soder([Marker.END01;Marker.END02;Marker.END03;Marker.END04],[Marker2.END01;Marker2.END02;Marker2.END03;Marker2.END04])

figure;
hold on;
axis equal;
plot3(Marker.END01(1),Marker.END01(2),Marker.END01(3),'Marker','o','Color','green');
plot3(Marker.END02(1),Marker.END02(2),Marker.END02(3),'Marker','o','Color','red');
plot3(Marker.END03(1),Marker.END03(2),Marker.END03(3),'Marker','o','Color','red');
plot3(Marker.END04(1),Marker.END04(2),Marker.END04(3),'Marker','o','Color','red');
plot3(Marker2.END01(1),Marker2.END01(2),Marker2.END01(3),'Marker','x','Color','green');
plot3(Marker2.END02(1),Marker2.END02(2),Marker2.END02(3),'Marker','x','Color','blue');
plot3(Marker2.END03(1),Marker2.END03(2),Marker2.END03(3),'Marker','x','Color','blue');
plot3(Marker2.END04(1),Marker2.END04(2),Marker2.END04(3),'Marker','x','Color','blue');
Test1 = (R2*Marker.END01'+d2)';
Test2 = (R2*Marker.END02'+d2)';
Test3 = (R2*Marker.END03'+d2)';
Test4 = (R2*Marker.END04'+d2)';
plot3(Test1(1),Test1(2),Test1(3),'Marker','.','Color','green');
plot3(Test2(1),Test2(2),Test2(3),'Marker','.','Color','red');
plot3(Test3(1),Test3(2),Test3(3),'Marker','.','Color','red');
plot3(Test4(1),Test4(2),Test4(3),'Marker','.','Color','red');