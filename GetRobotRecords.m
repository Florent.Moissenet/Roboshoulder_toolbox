% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/Roboshoulder_Toolbox
% Reference    : To be defined
% Date         : March 2020
% -------------------------------------------------------------------------
% Description  : To be defined
% -------------------------------------------------------------------------
% Dependencies : To be defined
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function Trial = GetRobotRecords(Folder,Trial)

% -------------------------------------------------------------------------
% SET CSV FILES RELATED TO ROBOT DATA
% -------------------------------------------------------------------------
cd(Folder.data);
temp      = dir(['*_',Trial.motion,'_',Trial.side(1),'*.csv']);
csvFiles1 = {};
csvFiles2 = {};
csvFiles3 = {};
csvFiles4 = {};
for i = 1:size(temp,1)
    
    % Set list of files related to joint angles
    if ~isempty(strfind(temp(i).name,'js'))%~isempty(strfind(temp(i).name,'joint-state'))%
        if isempty(strfind(Trial.direction,'reversed')) && isempty(strfind(temp(i).name,'reversed'))
            csvFiles1 = [csvFiles1 temp(i).name];
        elseif ~isempty(strfind(Trial.direction,'reversed')) && ~isempty(strfind(temp(i).name,'reversed'))
            csvFiles1 = [csvFiles1 temp(i).name];
        end
    end
    
    % Set list of files related to joint torques
    if ~isempty(strfind(temp(i).name,'jt'))%~isempty(strfind(temp(i).name,'joint-torque'))%
        if isempty(strfind(Trial.direction,'reversed')) && isempty(strfind(temp(i).name,'reversed'))
            csvFiles2 = [csvFiles2 temp(i).name];
        elseif ~isempty(strfind(Trial.direction,'reversed')) && ~isempty(strfind(temp(i).name,'reversed'))
            csvFiles2 = [csvFiles2 temp(i).name];
        end
    end
    
    % Set list of files related to joint external torques
    if ~isempty(strfind(temp(i).name,'jet'))%~isempty(strfind(temp(i).name,'joint-external-torque'))%
        if isempty(strfind(Trial.direction,'reversed')) && isempty(strfind(temp(i).name,'reversed'))
            csvFiles3 = [csvFiles3 temp(i).name];
        elseif ~isempty(strfind(Trial.direction,'reversed')) && ~isempty(strfind(temp(i).name,'reversed'))
            csvFiles3 = [csvFiles3 temp(i).name];
        end
    end
    
    % Set list of files related to the cartesian pose
    if ~isempty(strfind(temp(i).name,'cp'))%~isempty(strfind(temp(i).name,'cartesian-pose'))%
        if isempty(strfind(Trial.direction,'reversed')) && isempty(strfind(temp(i).name,'reversed'))
            csvFiles4 = [csvFiles4 temp(i).name];
        elseif ~isempty(strfind(Trial.direction,'reversed')) && ~isempty(strfind(temp(i).name,'reversed'))
            csvFiles4 = [csvFiles4 temp(i).name];
        end
    end
end
Trial.Robot.fileRecord = [csvFiles1 csvFiles2 csvFiles3 csvFiles4];
clear temp;

% -------------------------------------------------------------------------
% EXTRACT ROBOT DATA FROM CSV FILES (MERGED RAW CYCLES)
% -------------------------------------------------------------------------

% Store raw joint angles and velocities merged across cycles (stored at 100 Hz)
frames = 0;
for i = 1:size(csvFiles1,2)
    
    % Extract data
    temp = readtable(csvFiles1{i});
    n    = size(temp,1);
    
    % Joint angles (rad)
    for j = 1:8
        Trial.Robot.Joint(j).Angle.n = n;
        Trial.Robot.Joint(j).Angle.f = 100;
    end
    Trial.Robot.Joint(1).Angle.raw = [Trial.Robot.Joint(1).Angle.raw; temp{:,4}];
    Trial.Robot.Joint(2).Angle.raw = [Trial.Robot.Joint(2).Angle.raw; temp{:,8}];
    Trial.Robot.Joint(3).Angle.raw = [Trial.Robot.Joint(3).Angle.raw; temp{:,12}];
    Trial.Robot.Joint(4).Angle.raw = [Trial.Robot.Joint(4).Angle.raw; temp{:,16}];
    Trial.Robot.Joint(5).Angle.raw = [Trial.Robot.Joint(5).Angle.raw; temp{:,20}];
    Trial.Robot.Joint(6).Angle.raw = [Trial.Robot.Joint(6).Angle.raw; temp{:,24}];
    Trial.Robot.Joint(7).Angle.raw = [Trial.Robot.Joint(7).Angle.raw; temp{:,28}];
    Trial.Robot.Joint(8).Angle.raw = [Trial.Robot.Joint(8).Angle.raw; zeros(n,1,1)];
    
    % Joint velocities (rad.s-1)
    for j = 1:8
        Trial.Robot.Joint(j).Velocity.n = n;
        Trial.Robot.Joint(j).Velocity.f = 100;
    end
    Trial.Robot.Joint(1).Velocity.raw = [Trial.Robot.Joint(1).Velocity.raw; temp{:,5}];
    Trial.Robot.Joint(2).Velocity.raw = [Trial.Robot.Joint(2).Velocity.raw; temp{:,9}];
    Trial.Robot.Joint(3).Velocity.raw = [Trial.Robot.Joint(3).Velocity.raw; temp{:,13}];
    Trial.Robot.Joint(4).Velocity.raw = [Trial.Robot.Joint(4).Velocity.raw; temp{:,17}];
    Trial.Robot.Joint(5).Velocity.raw = [Trial.Robot.Joint(5).Velocity.raw; temp{:,21}];
    Trial.Robot.Joint(6).Velocity.raw = [Trial.Robot.Joint(6).Velocity.raw; temp{:,25}];
    Trial.Robot.Joint(7).Velocity.raw = [Trial.Robot.Joint(7).Velocity.raw; temp{:,29}];
    Trial.Robot.Joint(8).Velocity.raw = [Trial.Robot.Joint(8).Velocity.raw; zeros(n,1,1)];
        
    % Get estimated cycle events
    frames             = frames+n;
    Trial.Robot.Event1 = [Trial.Robot.Event1 frames];
    
    clear temp n; 
        
end
clear frames

% Store raw joint motor torques merged across cycles (stored at ?? Hz)
frames = 0;
for i = 1:size(csvFiles2,2)
    
    temp = readtable(csvFiles2{i});
    n    = size(temp,1);
    
    % Joint motor torques (Nm)
    for j = 1:8
        Trial.Robot.Joint(j).MotTorque.n = n;
        Trial.Robot.Joint(j).MotTorque.f = 'async';
    end
    Trial.Robot.Joint(1).MotTorque.raw = [Trial.Robot.Joint(1).MotTorque.raw; temp{:,3}];
    Trial.Robot.Joint(2).MotTorque.raw = [Trial.Robot.Joint(2).MotTorque.raw; temp{:,4}];
    Trial.Robot.Joint(3).MotTorque.raw = [Trial.Robot.Joint(3).MotTorque.raw; temp{:,5}];
    Trial.Robot.Joint(4).MotTorque.raw = [Trial.Robot.Joint(4).MotTorque.raw; temp{:,6}];
    Trial.Robot.Joint(5).MotTorque.raw = [Trial.Robot.Joint(5).MotTorque.raw; temp{:,7}];
    Trial.Robot.Joint(6).MotTorque.raw = [Trial.Robot.Joint(6).MotTorque.raw; temp{:,8}];
    Trial.Robot.Joint(7).MotTorque.raw = [Trial.Robot.Joint(7).MotTorque.raw; temp{:,9}];
    Trial.Robot.Joint(8).MotTorque.raw = [Trial.Robot.Joint(8).MotTorque.raw; zeros(n,1,1)];
    
    % Get estimated cycle events
    frames             = frames+n-1;
    Trial.Robot.Event2 = [Trial.Robot.Event2 frames];
    
    clear temp n; 
        
end
clear frames

% Store raw joint external torques merged across cycles (stored at ?? Hz)
% frames assumed to be same as for motor torque
for i = 1:size(csvFiles3,2)
    
    temp = readtable(csvFiles3{i});
    n    = size(temp,1);
    
    % Joint external torques (Nm)
    for j = 1:8
        Trial.Robot.Joint(j).ExtTorque.n = n;
        Trial.Robot.Joint(j).ExtTorque.f = 'async';
    end
    Trial.Robot.Joint(1).ExtTorque.raw = [Trial.Robot.Joint(1).ExtTorque.raw; temp{:,3}];
    Trial.Robot.Joint(2).ExtTorque.raw = [Trial.Robot.Joint(2).ExtTorque.raw; temp{:,4}];
    Trial.Robot.Joint(3).ExtTorque.raw = [Trial.Robot.Joint(3).ExtTorque.raw; temp{:,5}];
    Trial.Robot.Joint(4).ExtTorque.raw = [Trial.Robot.Joint(4).ExtTorque.raw; temp{:,6}];
    Trial.Robot.Joint(5).ExtTorque.raw = [Trial.Robot.Joint(5).ExtTorque.raw; temp{:,7}];
    Trial.Robot.Joint(6).ExtTorque.raw = [Trial.Robot.Joint(6).ExtTorque.raw; temp{:,8}];
    Trial.Robot.Joint(7).ExtTorque.raw = [Trial.Robot.Joint(7).ExtTorque.raw; temp{:,9}];
    Trial.Robot.Joint(8).ExtTorque.raw = [Trial.Robot.Joint(8).ExtTorque.raw; zeros(n,1,1)];
    
    clear temp n; 
        
end

% % Store raw cartesian pose of the flange merged across cycles (stored at ?? Hz)
% % frames assumed to be same as for motor torque
% for i = 1:size(csvFiles3,2)
%     
%     temp = readtable(csvFiles3{i});
%     n    = size(temp,1);
%     
%     % Position of the flange (m)
%     Trial.Robot.Segment(9).Position.n   = n;
%     Trial.Robot.Segment(9).Position.f   = 'async';
%     Trial.Robot.Segment(9).Position.raw = [Trial.Robot.Segment(9).Position.raw; temp{:,3} temp{:,4} temp{:,5}];
%     
%     % Orientation of the flange (quaternion)
%     Trial.Robot.Segment(9).Orientation.n   = n;
%     Trial.Robot.Segment(9).Orientation.f   = 'async';
%     Trial.Robot.Segment(9).Orientation.raw = [Trial.Robot.Segment(9).Orientation.raw; temp{:,6} temp{:,7} temp{:,8} temp{:,9}];
%     
%     clear temp; 
%         
% end

clear csvFiles1 csvFiles2 csvFiles3 csvFiles4;

% -------------------------------------------------------------------------
% CROP AND TIME-NORMALISE DATA PER MOVEMENT CYCLE
% -------------------------------------------------------------------------

% Data related to Event1
k = 1;
for i = 1:size(Trial.Robot.Event1,2)-1
    start          = Trial.Robot.Event1(i)+1;
    stop           = Trial.Robot.Event1(i+1)-1;
    Cycle(k).start = start;
    Cycle(k).stop  = stop;
    k              = k+1;    
    clear start stop;
end
for i = 1:size(Trial.Robot.Joint,2)
    for j = 1:size(Cycle,2)
        n  = Cycle(j).stop-Cycle(j).start+1;
        k  = (1:n)';
        k0 = (linspace(1,n,101))';
        Trial.Robot.Joint(i).Angle.cycle(:,1,j)    = interp1(k,...
                                                             Trial.Robot.Joint(i).Angle.raw(Cycle(j).start:Cycle(j).stop,1),...
                                                             k0,'spline');
        Trial.Robot.Joint(i).Velocity.cycle(:,1,j) = interp1(k,...
                                                             Trial.Robot.Joint(i).Velocity.raw(Cycle(j).start:Cycle(j).stop,1),...
                                                             k0,'spline');
        clear n n0;
    end
end
clear Cycle k k0;

% Data related to Event2
k = 1;
for i = 1:size(Trial.Robot.Event2,2)-1
    start          = Trial.Robot.Event2(i)+10;
    stop           = Trial.Robot.Event2(i+1)-1;
    Cycle(k).start = start;
    Cycle(k).stop  = stop;
    k              = k+1;   
    clear start stop;
end
for i = 1:size(Trial.Robot.Joint,2)
    for j = 1:size(Cycle,2)
        n  = Cycle(j).stop-Cycle(j).start+1;
        k  = (1:n)';
        k0 = (linspace(1,n,101))';
        Trial.Robot.Joint(i).MotTorque.cycle(:,1,j) = interp1(k,...
                                                              Trial.Robot.Joint(i).MotTorque.raw(Cycle(j).start:Cycle(j).stop,1),...
                                                              k0,'spline');
        Trial.Robot.Joint(i).ExtTorque.cycle(:,1,j) = interp1(k,...
                                                              Trial.Robot.Joint(i).ExtTorque.raw(Cycle(j).start:Cycle(j).stop,1),...
                                                              k0,'spline');
        clear n n0;
    end
end
% for i = 9 % Segment(9)
%     for j = 1:size(Cycle,2)
%         n  = Cycle(j).stop-Cycle(j).start+1;
%         k  = (1:n)';
%         k0 = (linspace(1,n,101))';
%         Trial.Robot.Segment(i).Position.cycle(:,:,j)    = interp1(k,...
%                                                                   Trial.Robot.Segment(i).Position.raw(Cycle(j).start:Cycle(j).stop,:),...
%                                                                   k0,'spline');
%         Trial.Robot.Segment(i).Orientation.cycle(:,:,j) = interp1(k,...
%                                                                   Trial.Robot.Segment(i).Orientation.raw(Cycle(j).start:Cycle(j).stop,:),...
%                                                                   k0,'spline');
%         clear n n0;
%     end
% end