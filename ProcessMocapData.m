% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/Roboshoulder_Toolbox
% Reference    : To be defined
% Date         : March 2020
% -------------------------------------------------------------------------
% Description  : To be defined
% -------------------------------------------------------------------------
% Dependencies : To be defined
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% INIT THE WORKSPACE
% -------------------------------------------------------------------------
clearvars;
close all;
clc;

% -------------------------------------------------------------------------
% SET FOLDERS
% -------------------------------------------------------------------------
disp('Set folders');
Folder.toolbox      = 'C:\Users\moissene\Documents\Switchdrive\Projets\B-LAB\Projets\2019-2020 - ROBOSHOULDER\Donn�es\Roboshoulder_toolbox\';
Folder.dependencies = [Folder.toolbox,'dependencies\'];
Folder.data         = uigetdir;
Folder.export       = 'C:\Users\moissene\Documents\Switchdrive\Projets\B-LAB\Projets\2019-2020 - ROBOSHOULDER\Donn�es\RS001\Mocap\robot_motions\';
addpath(Folder.toolbox);
addpath(genpath(Folder.dependencies)); % Adds the specified folders to the top of the search path for the current session

% -------------------------------------------------------------------------
% SET SPECIMEN
% -------------------------------------------------------------------------
disp('Set specimen parameters');

% RS001
Specimen.reference = 'RS001';
Specimen.gender    = 'Female';
Specimen.age       = 94; % years
Specimen.height    = 158; % cm
Specimen.weight    = 56.4; % kg

% -------------------------------------------------------------------------
% DEFINE SESSION
% -------------------------------------------------------------------------
disp('Set session parameters');
Session.date     = '02-10-2020';
Session.examiner = 'FM';

% -------------------------------------------------------------------------
% LOAD C3D FILES
% -------------------------------------------------------------------------
disp('Extract data from C3D files');
cd(Folder.data);
% RS001 
conditionRight = {'ACn_CCn_MUn','ACs_CCn_MUs','ACs_CCs_MUs',...            % AC: AC ligaments, CC: CC ligaments, MU: muscles
                  'ACs_CCr_MUs_1','ACrV2_CCr_MUs','ACrV1_CCr_MUs',...      % n: native, s: sectionned; r:reconstructed
                  'ACrH_CCr_MUs','ACrH8_CCr_MUs','ACs_CCr2_MUs_2'};
conditionLeft  = {'ACn_CCn_MUn','ACs_CCn_MUs','ACs_CCs_MUs',...            % AC: AC ligaments, CC: CC ligaments, MU: muscles
                  'ACrV2_CCr_MUs','ACrV1_CCr_MUs','ACs_CCr_MUs_1',...      % n: native, s: sectionned; r:reconstructed
                  'ACrH_CCr_MUs','ACrH8_CCr_MUs'};              
c3dFiles       = dir('*.c3d');
kR             = 1;
for i = 1:size(c3dFiles)
    disp(['  - ',c3dFiles(i).name]);
    if strfind(c3dFiles(i).name,'Right') == 1
        Trial(kR).btk       = btkReadAcquisition(c3dFiles(i).name);
        Trial(kR).file      = c3dFiles(i).name;
        Trial(kR).side      = 'Right';
        Trial(kR).serie     = 'CC_first';
        if ~isempty(strfind(c3dFiles(i).name,'Static'))
            Trial(kR).condition = 'Static';
        else
            Trial(kR).condition = conditionRight{kR};
        end
        Trial(kR).n0        = btkGetFirstFrame(Trial(kR).btk);
        Trial(kR).n1        = btkGetLastFrame(Trial(kR).btk)-Trial(kR).n0+1;
        Trial(kR).fmarker   = btkGetPointFrequency(Trial(kR).btk);
        kR                  = kR+1; % Assume that RightStatic is listed after RightCondition in c3dFiles.name
    elseif strfind(c3dFiles(i).name,'Left') == 1
        Trial(kL).btk       = btkReadAcquisition(c3dFiles(i).name);
        Trial(kL).file      = c3dFiles(i).name;
        Trial(kL).side      = 'Left';
        Trial(kL).serie     = 'AC_first';
        if ~isempty(strfind(c3dFiles(i).name,'Static'))
            Trial(kL).condition = 'Static';
        else
            Trial(kL).condition = conditionLeft{kL};
        end
        Trial(kL).n0        = btkGetFirstFrame(Trial(kL).btk);
        Trial(kL).n1        = btkGetLastFrame(Trial(kL).btk)-Trial(kL).n0+1;
        Trial(kL).fmarker   = btkGetPointFrequency(Trial(kL).btk);
        kL                  = kL+1; % Assume that LeftStatic is listed after LeftCondition in c3dFiles.name
    end
end
clear i kR kL c3dFiles conditionRight conditionLeft;

% -------------------------------------------------------------------------
% PRE-PROCESS STATIC DATA
% -------------------------------------------------------------------------
disp('Pre-process static data');
for i = 1:size(Trial,2)    
    if strcmp(Trial(i).condition,'Static')

        disp(['  - ',Trial(i).file]);

        % Marker trajectories
        Marker          = btkGetMarkers(Trial(i).btk);
        Trial(i).Marker = [];
        Trial(i)        = InitialiseMarkerset(Trial(i),Marker);
        Trial(i)        = ProcessMarkerTrajectories([],Trial(i));
        clear Marker;

        % Store processed data in a new C3D file
        ExportC3D(Trial(i),Specimen,Session,Folder);
    
    end
end

% -------------------------------------------------------------------------
% PRE-PROCESS TRIAL DATA
% -------------------------------------------------------------------------
disp('Pre-process trial data');
for i = 1:size(Trial,2)
    if ~strcmp(Trial(i).condition,'Static')
    
        disp(['  - ',Trial(i).file]);
        
        % Find related static file
        for j = 1:size(Trial,2)
            if strcmp(Trial(j).condition,'Static') == 1 && ...
               strcmp(Trial(j).side,Trial(i).side) == 1
                Static = Trial(j);
            end
        end

        % Prepare marker trajectories
        Marker               = btkGetMarkers(Trial(i).btk);
        Trial(i).Marker      = [];
        Trial(i)             = InitialiseMarkerset(Trial(i),Marker);
        fmethod.type         = 'intercor';
        fmethod.gapThreshold = [];
        smethod.type         = 'movmean';
        smethod.parameter    = 100;        
        Trial(i)             = ProcessMarkerTrajectories(Static,Trial(i),fmethod,smethod);        
        clear Static Marker;
        
        % Crop marker trajectories per movement cycle
        Event          = btkGetEvents(Trial(1).btk);
        motionList     = {'FE','AA','IER','HFE','VT','HC'};
        for j = 1:size(motionList,2)
            Trial(i) = CropMarkerTrajectories(Trial(i),Event,motionList,j);
        end 

        % Store processed data in a new C3D file
        ExportC3D(Trial(i),Specimen,Session,Folder);
    
    end
end
%%
nMarker = 22;
figure; hold on;
plot(1:101,Trial(1).Marker(nMarker).Trajectory.cycle(:,1,1),'red');
plot(1:101,Trial(1).Marker(nMarker).Trajectory.cycle(:,1,3),'red');
plot(1:101,Trial(1).Marker(nMarker).Trajectory.cycle(:,1,5),'red');
plot(102:202,Trial(1).Marker(nMarker).Trajectory.cycle(:,1,2),'blue');
plot(102:202,Trial(1).Marker(nMarker).Trajectory.cycle(:,1,4),'blue');
plot(102:202,Trial(1).Marker(nMarker).Trajectory.cycle(:,1,6),'blue');

ECART ENTRE TRAJECTOIRES = TRAITEMENT MARQUEUR OU ERREUR ROBOT?
TESTER EN NE LISSANT PAS LES TRAJECTOIRES