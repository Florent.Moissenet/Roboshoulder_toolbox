% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/Roboshoulder_Toolbox
% Reference    : To be defined
% Date         : March 2020
% -------------------------------------------------------------------------
% Description  : To be defined
% -------------------------------------------------------------------------
% Dependencies : To be defined
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function Quality = AssessDataQuality(Quality,Trial,ijoint,isegment,nspecimen,ispecimen,nside,iside,nmotion,imotion,ndirection,idirection)

% -------------------------------------------------------------------------
% ROBOT JOINT ANGLE RELIABILITY AND VALIDITY
% -------------------------------------------------------------------------
if ~isempty(ijoint)
    Measures   = [];
    Reference  = [];

    for i = 1:size(Trial,2)
        if strcmp(Trial(i).specimen,nspecimen{ispecimen})
            if strcmp(Trial(i).side,nside{iside})
                if strcmp(Trial(i).motion,nmotion{imotion})
                    if strcmp(Trial(i).direction,ndirection{idirection})
                        Measures  = [Measures ...
                                     Trial(i).Robot.Joint(ijoint).Angle.cycle(:,1,1)*180/pi ...
                                     Trial(i).Robot.Joint(ijoint).Angle.cycle(:,1,2)*180/pi ...
                                     Trial(i).Robot.Joint(ijoint).Angle.cycle(:,1,3)*180/pi];
                        Reference = Trial(i).Robot.Joint(ijoint).Angle.ref(:,1)*180/pi;
                    end
                end
            end
        end
    end

    Average = mean(Measures,2);
    cDTWr = [];
    cDTWv = [];
    for i = 1:size(Measures,2)
        % Resample the vectors to avoid discontinuities due to time warping
        nframes = 101;
        vec1    = interp1((1:length(Measures(:,i)))',Measures(:,i),(linspace(1,length(Measures(:,i)),nframes))');
        vec2    = interp1((1:length(Average))',Average,(linspace(1,length(Average),nframes))');
        vec3    = interp1((1:length(Reference))',Reference,(linspace(1,length(Reference),nframes))');
        % Compute mean euclidean distance between signals using dynamic
        % time warping
        cDTWr = [cDTWr sqrt(dtw(vec1,vec2,'squared')/nframes)];
        cDTWv = [cDTWv sqrt(dtw(vec1,vec3,'squared')/nframes)];
    end
    % Store quality parameters across similar trials
    Quality.Robot.Joint(ijoint).Angle.(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWr_all  = cDTWr;
    Quality.Robot.Joint(ijoint).Angle.(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWr_mean = mean(cDTWr);
    Quality.Robot.Joint(ijoint).Angle.(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWr_std  = std(cDTWr);
    Quality.Robot.Joint(ijoint).Angle.(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWv_all  = cDTWv;
    Quality.Robot.Joint(ijoint).Angle.(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWv_mean = mean(cDTWv);
    Quality.Robot.Joint(ijoint).Angle.(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWv_std  = std(cDTWv);
end

% -------------------------------------------------------------------------
% ROBOT TERMINAL ELEMENT POSITION AND ORIENTATION RELIABILITY AND VALIDITY
% -------------------------------------------------------------------------
if ~isempty(isegment)
    ICS          = [1 0 0; 0 1 0; 0 0 1];
    MeasuresPX   = [];
    MeasuresPY   = [];
    MeasuresPZ   = [];
    MeasuresRXY  = [];
    MeasuresRZ   = [];
    ReferencePX  = [];
    ReferencePY  = [];
    ReferencePZ  = [];
    ReferenceRXY = [];
    ReferenceRZ  = [];
    
    for i = 1:size(Trial,2)
        if strcmp(Trial(i).specimen,nspecimen{ispecimen})
            if strcmp(Trial(i).side,nside{iside})
                if strcmp(Trial(i).motion,nmotion{imotion})
                    if strcmp(Trial(i).direction,ndirection{idirection})   
                        
                        % Remove non-moving initial frames (based on X
                        % direction)
                        start1 = 1;
                        data1  = Trial(i).Robot.Segment(isegment).T.cycle(:,1,4,1)*1e3;
                        for j = 1:size(data1,1)-1
                            if abs(data1(j+1,:)-data1(j,:)) < 0.1 % mm
                                start1 = j;
                            else
                                break;
                            end
                        end
                        start2 = 1;
                        data2  = Trial(i).Robot.Segment(isegment).T.cycle(:,1,4,2)*1e3;
                        for j = 1:size(data2,1)-1
                            if abs(data2(j+1,:)-data2(j,:)) < 0.1 % mm
                                start2 = j;
                            else
                                break;
                            end
                        end
                        start3 = 1;
                        data3  = Trial(i).Robot.Segment(isegment).T.cycle(:,1,4,3)*1e3;
                        for j = 1:size(data3,1)-1
                            if abs(data3(j+1,:)-data3(j,:)) < 0.1 % mm
                                start3 = j;
                            else
                                break;
                            end
                        end
                        clear data1 data2 data3 j;
                        
                        % Remove non-moving last frames (based on X
                        % direction)
                        stop1 = 1;
                        data1  = Trial(i).Robot.Segment(isegment).T.cycle(:,1,4,1)*1e3;
                        for j = size(data1,1)-1:-1:1
                            if abs(data1(j+1,:)-data1(j,:)) < 0.1 % mm
                                stop1 = j;
                            else
                                break;
                            end
                        end
                        stop2 = 1;
                        data2  = Trial(i).Robot.Segment(isegment).T.cycle(:,1,4,2)*1e3;
                        for j = size(data2,1)-1:-1:1
                            if abs(data2(j+1,:)-data2(j,:)) < 0.1 % mm
                                stop2 = j;
                            else
                                break;
                            end
                        end
                        stop3 = 1;
                        data3  = Trial(i).Robot.Segment(isegment).T.cycle(:,1,4,3)*1e3;
                        for j = size(data3,1)-1:-1:1
                            if abs(data3(j+1,:)-data3(j,:)) < 0.1 % mm
                                stop3 = j;
                            else
                                break;
                            end
                        end
                        clear data1 data2 data3 j;
                        
                        % Store data reasmpled to nframes
                        nframes = 5e3;
                        MeasuresPX  = [MeasuresPX ...
                                       interp1((1:length(Trial(i).Robot.Segment(isegment).T.cycle(start1:stop1,1,4,1)))',Trial(i).Robot.Segment(isegment).T.cycle(start1:stop1,1,4,1)*1e3,(linspace(1,length(Trial(i).Robot.Segment(isegment).T.cycle(start1:stop1,1,4,1)),nframes))') ...
                                       interp1((1:length(Trial(i).Robot.Segment(isegment).T.cycle(start2:stop2,1,4,2)))',Trial(i).Robot.Segment(isegment).T.cycle(start2:stop2,1,4,2)*1e3,(linspace(1,length(Trial(i).Robot.Segment(isegment).T.cycle(start2:stop2,1,4,2)),nframes))') ...
                                       interp1((1:length(Trial(i).Robot.Segment(isegment).T.cycle(start3:stop3,1,4,3)))',Trial(i).Robot.Segment(isegment).T.cycle(start3:stop3,1,4,3)*1e3,(linspace(1,length(Trial(i).Robot.Segment(isegment).T.cycle(start3:stop3,1,4,3)),nframes))')];
                        MeasuresPY  = [MeasuresPY ...
                                       interp1((1:length(Trial(i).Robot.Segment(isegment).T.cycle(start1:stop1,2,4,1)))',Trial(i).Robot.Segment(isegment).T.cycle(start1:stop1,2,4,1)*1e3,(linspace(1,length(Trial(i).Robot.Segment(isegment).T.cycle(start1:stop1,2,4,1)),nframes))') ...
                                       interp1((1:length(Trial(i).Robot.Segment(isegment).T.cycle(start2:stop2,2,4,2)))',Trial(i).Robot.Segment(isegment).T.cycle(start2:stop2,2,4,2)*1e3,(linspace(1,length(Trial(i).Robot.Segment(isegment).T.cycle(start2:stop2,2,4,2)),nframes))') ...
                                       interp1((1:length(Trial(i).Robot.Segment(isegment).T.cycle(start3:stop3,2,4,3)))',Trial(i).Robot.Segment(isegment).T.cycle(start3:stop3,2,4,3)*1e3,(linspace(1,length(Trial(i).Robot.Segment(isegment).T.cycle(start3:stop3,2,4,3)),nframes))')];
                        MeasuresPZ  = [MeasuresPZ ...
                                       interp1((1:length(Trial(i).Robot.Segment(isegment).T.cycle(start1:stop1,3,4,1)))',Trial(i).Robot.Segment(isegment).T.cycle(start1:stop1,3,4,1)*1e3,(linspace(1,length(Trial(i).Robot.Segment(isegment).T.cycle(start1:stop1,3,4,1)),nframes))') ...
                                       interp1((1:length(Trial(i).Robot.Segment(isegment).T.cycle(start2:stop2,3,4,2)))',Trial(i).Robot.Segment(isegment).T.cycle(start2:stop2,3,4,2)*1e3,(linspace(1,length(Trial(i).Robot.Segment(isegment).T.cycle(start2:stop2,3,4,2)),nframes))') ...
                                       interp1((1:length(Trial(i).Robot.Segment(isegment).T.cycle(start3:stop3,3,4,3)))',Trial(i).Robot.Segment(isegment).T.cycle(start3:stop3,3,4,3)*1e3,(linspace(1,length(Trial(i).Robot.Segment(isegment).T.cycle(start3:stop3,3,4,3)),nframes))')];
                        k = 1;
                        for iframe = start1:stop1
                            tempXY1(k,:) = rad2deg(atan2(norm(cross(permute(Trial(i).Robot.Segment(isegment).T.cycle(iframe,1:3,1,1),[2,3,1,4]),ICS(:,1))), dot(permute(Trial(i).Robot.Segment(isegment).T.cycle(iframe,1:3,1,1),[2,3,1,4]),ICS(:,1))));
                            tempZ1(k,:)  = rad2deg(atan2(norm(cross(permute(Trial(i).Robot.Segment(isegment).T.cycle(iframe,1:3,3,1),[2,3,1,4]),ICS(:,3))), dot(permute(Trial(i).Robot.Segment(isegment).T.cycle(iframe,1:3,3,1),[2,3,1,4]),ICS(:,3))));
                            k            = k+1;
                        end
                        k = 1;
                        for iframe = start2:stop2
                            tempXY2(k,:) = rad2deg(atan2(norm(cross(permute(Trial(i).Robot.Segment(isegment).T.cycle(iframe,1:3,1,2),[2,3,1,4]),ICS(:,1))), dot(permute(Trial(i).Robot.Segment(isegment).T.cycle(iframe,1:3,1,2),[2,3,1,4]),ICS(:,1))));
                            tempZ2(k,:)  = rad2deg(atan2(norm(cross(permute(Trial(i).Robot.Segment(isegment).T.cycle(iframe,1:3,3,2),[2,3,1,4]),ICS(:,3))), dot(permute(Trial(i).Robot.Segment(isegment).T.cycle(iframe,1:3,3,2),[2,3,1,4]),ICS(:,3))));
                            k            = k+1;
                        end
                        k = 1;
                        for iframe = start3:stop3
                            tempXY3(k,:) = rad2deg(atan2(norm(cross(permute(Trial(i).Robot.Segment(isegment).T.cycle(iframe,1:3,1,3),[2,3,1,4]),ICS(:,1))), dot(permute(Trial(i).Robot.Segment(isegment).T.cycle(iframe,1:3,1,3),[2,3,1,4]),ICS(:,1))));
                            tempZ3(k,:)  = rad2deg(atan2(norm(cross(permute(Trial(i).Robot.Segment(isegment).T.cycle(iframe,1:3,3,3),[2,3,1,4]),ICS(:,3))), dot(permute(Trial(i).Robot.Segment(isegment).T.cycle(iframe,1:3,3,3),[2,3,1,4]),ICS(:,3))));
                            k            = k+1;
                        end
                        MeasuresRXY = [MeasuresRXY ...
                                       interp1((1:length(tempXY1))',tempXY1,(linspace(1,length(tempXY1),nframes))') ...
                                       interp1((1:length(tempXY2))',tempXY2,(linspace(1,length(tempXY2),nframes))') ...
                                       interp1((1:length(tempXY3))',tempXY3,(linspace(1,length(tempXY3),nframes))')];
                        MeasuresRZ  = [MeasuresRZ ...
                                       interp1((1:length(tempZ1))',tempZ1,(linspace(1,length(tempZ1),nframes))') ...
                                       interp1((1:length(tempZ2))',tempZ2,(linspace(1,length(tempZ2),nframes))') ...
                                       interp1((1:length(tempZ3))',tempZ3,(linspace(1,length(tempZ3),nframes))')];
                        clear k tempXY1 tempZ1 tempXY2 tempZ2 tempXY3 tempZ3;
                                                
                        ReferencePX  = interp1((1:length(Trial(i).Robot.Segment(9).Position.ref(:,1)))',Trial(i).Robot.Segment(9).Position.ref(:,1)*1e3,(linspace(1,length(Trial(i).Robot.Segment(9).Position.ref(:,1)),nframes))');
                        ReferencePY  = interp1((1:length(Trial(i).Robot.Segment(9).Position.ref(:,2)))',Trial(i).Robot.Segment(9).Position.ref(:,2)*1e3,(linspace(1,length(Trial(i).Robot.Segment(9).Position.ref(:,2)),nframes))');
                        ReferencePZ  = interp1((1:length(Trial(i).Robot.Segment(9).Position.ref(:,3)))',Trial(i).Robot.Segment(9).Position.ref(:,3)*1e3,(linspace(1,length(Trial(i).Robot.Segment(9).Position.ref(:,3)),nframes))');
                        ReferenceRXY = interp1((1:length(Trial(i).Robot.Segment(9).OrientationXY.ref(:,1)))',Trial(i).Robot.Segment(9).OrientationXY.ref(:,1),(linspace(1,length(Trial(i).Robot.Segment(9).OrientationXY.ref(:,1)),nframes))');
                        ReferenceRZ  = interp1((1:length(Trial(i).Robot.Segment(9).OrientationZ.ref(:,1)))',Trial(i).Robot.Segment(9).OrientationZ.ref(:,1),(linspace(1,length(Trial(i).Robot.Segment(9).OrientationZ.ref(:,1)),nframes))');
                        
                    end
                end
            end
        end
    end

    AveragePX  = mean(MeasuresPX,2);
    cDTWr = [];
    cDTWv = [];
    for i = 1:size(MeasuresPX,2)
        % Set vectors
        vec1 = MeasuresPX(:,i);
        vec2 = AveragePX;
        if idirection == 2
            vec3    = flip(interp1((1:length(ReferencePX))',ReferencePX,(linspace(1,length(ReferencePX),nframes))'));
        else
            vec3    = interp1((1:length(ReferencePX))',ReferencePX,(linspace(1,length(ReferencePX),nframes))');
        end
        % Compute mean euclidean distance between signals using dynamic
        % time warping
        cDTWr = [cDTWr sqrt(dtw(vec1,vec2,'squared')/nframes)];
        cDTWv = [cDTWv sqrt(dtw(vec1,vec3,'squared')/nframes)];
    end
    % Store mean quality parameters across similar trials
    Quality.Robot.Segment(isegment).Position(1).(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWr_all  = cDTWr;
    Quality.Robot.Segment(isegment).Position(1).(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWr_mean = mean(cDTWr);
    Quality.Robot.Segment(isegment).Position(1).(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWr_std  = std(cDTWr);
    Quality.Robot.Segment(isegment).Position(1).(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWv_all  = cDTWv;
    Quality.Robot.Segment(isegment).Position(1).(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWv_mean = mean(cDTWv);
    Quality.Robot.Segment(isegment).Position(1).(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWv_std  = std(cDTWv);
    clear vec1 vec2 vec3 cDTWr cDTWv;
    
    AveragePY  = mean(MeasuresPY,2);
    cDTWr = [];
    cDTWv = [];
    for i = 1:size(MeasuresPY,2)
        % Resample the vectors to avoid discontinuities due to time warping
        nframes = 101;
        vec1    = interp1((1:length(MeasuresPY(:,i)))',MeasuresPY(:,i),(linspace(1,length(MeasuresPY(:,i)),nframes))');
        vec2    = interp1((1:length(AveragePY))',AveragePY,(linspace(1,length(AveragePY),nframes))');
        if idirection == 2
            vec3    = flip(interp1((1:length(ReferencePY))',ReferencePY,(linspace(1,length(ReferencePY),nframes))'));
        else
            vec3    = interp1((1:length(ReferencePY))',ReferencePY,(linspace(1,length(ReferencePY),nframes))');
        end
        % Compute mean euclidean distance between signals using dynamic
        % time warping
        cDTWr = [cDTWr sqrt(dtw(vec1,vec2,'squared')/nframes)];
        cDTWv = [cDTWv sqrt(dtw(vec1,vec3,'squared')/nframes)];
    end
    % Store mean quality parameters across similar trials
    Quality.Robot.Segment(isegment).Position(2).(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWr_all  = cDTWr;
    Quality.Robot.Segment(isegment).Position(2).(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWr_mean = mean(cDTWr);
    Quality.Robot.Segment(isegment).Position(2).(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWr_std  = std(cDTWr);
    Quality.Robot.Segment(isegment).Position(2).(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWv_all  = cDTWv;
    Quality.Robot.Segment(isegment).Position(2).(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWv_mean = mean(cDTWv);
    Quality.Robot.Segment(isegment).Position(2).(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWv_std  = std(cDTWv);
    clear vec1 vec2 vec3 cDTWr cDTWv;
    
    AveragePZ  = mean(MeasuresPZ,2);
    cDTWr = [];
    cDTWv = [];
    for i = 1:size(MeasuresPZ,2)
        % Resample the vectors to avoid discontinuities due to time warping
        nframes = 101;
        vec1    = interp1((1:length(MeasuresPZ(:,i)))',MeasuresPZ(:,i),(linspace(1,length(MeasuresPZ(:,i)),nframes))');
        vec2    = interp1((1:length(AveragePZ))',AveragePZ,(linspace(1,length(AveragePZ),nframes))');
        if idirection == 2
            vec3    = flip(interp1((1:length(ReferencePZ))',ReferencePZ,(linspace(1,length(ReferencePZ),nframes))'));
        else
            vec3    = interp1((1:length(ReferencePZ))',ReferencePZ,(linspace(1,length(ReferencePZ),nframes))');
        end
        % Compute mean euclidean distance between signals using dynamic
        % time warping
        cDTWr = [cDTWr sqrt(dtw(vec1,vec2,'squared')/nframes)];
        cDTWv = [cDTWv sqrt(dtw(vec1,vec3,'squared')/nframes)];
    end
    % Store mean quality parameters across similar trials
    Quality.Robot.Segment(isegment).Position(3).(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWr_all  = cDTWr;
    Quality.Robot.Segment(isegment).Position(3).(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWr_mean = mean(cDTWr);
    Quality.Robot.Segment(isegment).Position(3).(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWr_std  = std(cDTWr);
    Quality.Robot.Segment(isegment).Position(3).(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWv_all  = cDTWv;
    Quality.Robot.Segment(isegment).Position(3).(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWv_mean = mean(cDTWv);
    Quality.Robot.Segment(isegment).Position(3).(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWv_std  = std(cDTWv);
    clear vec1 vec2 vec3 cDTWr cDTWv;
    
    AverageRXY  = mean(MeasuresRXY,2);
    cDTWr = [];
    cDTWv = [];
    for i = 1:size(MeasuresRXY,2)
        % Resample the vectors to avoid discontinuities due to time warping
        nframes = 101;
        vec1    = interp1((1:length(MeasuresRXY(:,i)))',MeasuresRXY(:,i),(linspace(1,length(MeasuresRXY(:,i)),nframes))');
        vec2    = interp1((1:length(AverageRXY))',AverageRXY,(linspace(1,length(AverageRXY),nframes))');
        if ispecimen == 1
            if idirection == 2
                if iside == 1
                    vec3 = flip(interp1((1:length(ReferenceRXY))',ReferenceRXY,(linspace(1,length(ReferenceRXY),nframes))'));
                else
                    vec3 = flip(interp1((1:length(ReferenceRXY))',180-ReferenceRXY,(linspace(1,length(ReferenceRXY),nframes))'));
                end
            else
                if iside == 1
                    vec3 = interp1((1:length(ReferenceRXY))',ReferenceRXY,(linspace(1,length(ReferenceRXY),nframes))');
                else
                    vec3 = interp1((1:length(ReferenceRXY))',180-ReferenceRXY,(linspace(1,length(ReferenceRXY),nframes))');
                end
            end
        else
            if idirection == 2
                if iside == 1
                    vec3 = flip(interp1((1:length(ReferenceRXY))',ReferenceRXY,(linspace(1,length(ReferenceRXY),nframes))'));
                else
                    vec3 = flip(interp1((1:length(ReferenceRXY))',ReferenceRXY,(linspace(1,length(ReferenceRXY),nframes))'));
                end
            else
                if iside == 1
                    vec3 = interp1((1:length(ReferenceRXY))',ReferenceRXY,(linspace(1,length(ReferenceRXY),nframes))');
                else
                    vec3 = interp1((1:length(ReferenceRXY))',ReferenceRXY,(linspace(1,length(ReferenceRXY),nframes))');
                end
            end
        end
        % Compute mean euclidean distance between signals using dynamic
        % time warping
        cDTWr = [cDTWr sqrt(dtw(vec1,vec2,'squared')/nframes)];
        cDTWv = [cDTWv sqrt(dtw(vec1,vec3,'squared')/nframes)];
    end
    % Store mean quality parameters across similar trials
    Quality.Robot.Segment(isegment).OrientationXY.(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWr_all  = cDTWr;
    Quality.Robot.Segment(isegment).OrientationXY.(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWr_mean = mean(cDTWr);
    Quality.Robot.Segment(isegment).OrientationXY.(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWr_std  = std(cDTWr);
    Quality.Robot.Segment(isegment).OrientationXY.(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWv_all  = cDTWv;
    Quality.Robot.Segment(isegment).OrientationXY.(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWv_mean = mean(cDTWv);
    Quality.Robot.Segment(isegment).OrientationXY.(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWv_std  = std(cDTWv);
    clear vec1 vec2 vec3 cDTWr cDTWv;
    
    AverageRZ  = mean(MeasuresRZ,2);
    cDTWr = [];
    cDTWv = [];
    for i = 1:size(MeasuresRZ,2)
        % Resample the vectors to avoid discontinuities due to time warping
        nframes = 101;
        vec1    = interp1((1:length(MeasuresRZ(:,i)))',MeasuresRZ(:,i),(linspace(1,length(MeasuresRZ(:,i)),nframes))');
        vec2    = interp1((1:length(AverageRZ))',AverageRZ,(linspace(1,length(AverageRZ),nframes))');
        if idirection == 2
            vec3    = flip(interp1((1:length(ReferenceRZ))',ReferenceRZ,(linspace(1,length(ReferenceRZ),nframes))'));
        else
            vec3    = interp1((1:length(ReferenceRZ))',ReferenceRZ,(linspace(1,length(ReferenceRZ),nframes))');
        end
        % Compute mean euclidean distance between signals using dynamic
        % time warping
        cDTWr = [cDTWr sqrt(dtw(vec1,vec2,'squared')/nframes)];
        cDTWv = [cDTWv sqrt(dtw(vec1,vec3,'squared')/nframes)];
    end
    % Store mean quality parameters across similar trials
    Quality.Robot.Segment(isegment).OrientationZ.(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWr_all  = cDTWr;
    Quality.Robot.Segment(isegment).OrientationZ.(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWr_mean = mean(cDTWr);
    Quality.Robot.Segment(isegment).OrientationZ.(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWr_std  = std(cDTWr);
    Quality.Robot.Segment(isegment).OrientationZ.(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWv_all  = cDTWv;
    Quality.Robot.Segment(isegment).OrientationZ.(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWv_mean = mean(cDTWv);
    Quality.Robot.Segment(isegment).OrientationZ.(nside{iside}).(nmotion{imotion}).(ndirection{idirection}).DTWv_std  = std(cDTWv);
    clear vec1 vec2 vec3 cDTWr cDTWv;
end