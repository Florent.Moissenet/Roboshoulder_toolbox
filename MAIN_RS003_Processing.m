% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/Roboshoulder_Toolbox
% Reference    : To be defined
% Date         : March 2020
% -------------------------------------------------------------------------
% Description  : To be defined
% -------------------------------------------------------------------------
% Dependencies : To be defined
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% INIT THE WORKSPACE
% -------------------------------------------------------------------------
clearvars;
close all;
clc;

% -------------------------------------------------------------------------
% SET FOLDERS
% -------------------------------------------------------------------------
Folder.toolbox      = 'C:\Users\moissene\Documents\Switchdrive\Projets\B-LAB\Projets\2019-2020 - ROBOSHOULDER\Donn�es\Roboshoulder_toolbox\';
Folder.export       = 'C:\Users\moissene\Documents\Switchdrive\Projets\B-LAB\Projets\2019-2020 - ROBOSHOULDER\Donn�es\RS003\Processed\';
Folder.dependencies = [Folder.toolbox,'dependencies\'];
addpath(Folder.toolbox);
addpath(genpath(Folder.dependencies)); % Adds the specified folders to the top of the search path for the current session

% -------------------------------------------------------------------------
% SET PARAMETERS
% -------------------------------------------------------------------------
nspecimen  = {'RS001','RS002','RS003','RS004','RS005'};
ntrial     = 1;
nside      = {'R','L'};
ndirection = {'normal','reversed'};

for ispecimen  = 3
for iside      = 1:size(nside,2)
    if iside == 1
        ncondition = {'N-N-N','L-N-N','L-N-L','L-L-L','L-R-L(1)',...       % AC-CC-Muscle, N: native, L: lesion, R: reconstructed
                      'Rver2-R-L','Rver1-R-L','L-R-L(2)',...
                      'Rhor-R-L','Rhor8-R-L',...                           % ver1: anterior vertical suture
                      'L-R-L(3)'};                                         % ver2: anterior+posterior vertical suture
        nmotion    = {'FE','AA','IER','HFE','VT','HC'};
    elseif iside == 2                                                      % hor: single horizontal suture
        ncondition = {'N-N-N','L-N-N','L-N-L','L-L-L','Rver2-R-L',...      % hor8: single horizontal suture + 8-shape suture
                      'Rver1-R-L','L-R-L(1)','Rhor-R-L','Rhor8-R-L',...
                      'L-R-L(2)'};                                         % (1) and (2): repetition of a similar condition
        nmotion    = {'FE','AA','IER','HFE','VT','HC'}; 
    end
for icondition = 1:size(ncondition,2)
for imotion    = 1:size(nmotion,2)
for idirection = 1:size(ndirection,2)
    
% -------------------------------------------------------------------------
% INITIALISE THE TRIAL STRUCTURE
% -------------------------------------------------------------------------
Trial(ntrial).specimen  = nspecimen{ispecimen};
Trial(ntrial).serie     = [];
Trial(ntrial).condition = ncondition{icondition};
Trial(ntrial).side      = nside{iside};
Trial(ntrial).motion    = nmotion{imotion};
Trial(ntrial).direction = ndirection{idirection};
if isempty(strfind(Trial(ntrial).side,'R'))
    Trial(ntrial).serie = 'CC_first';
elseif isempty(strfind(Trial(ntrial).side,'L'))
    Trial(ntrial).serie = 'AC_first';
end

% -------------------------------------------------------------------------
% INITIALISE THE ROBOT STRUCTURE
% -------------------------------------------------------------------------
Trial(ntrial).Robot.file   = [];
Trial(ntrial).Robot.Event1 = 1; % Related to joint angle and velocity
Trial(ntrial).Robot.Event2 = 1; % Related to other states
Trial(ntrial).Robot        = InitialiseRobot(Trial(ntrial).Robot,Folder);

% -------------------------------------------------------------------------
% GET ROBOT RECORDS
% -------------------------------------------------------------------------
if icondition < 10
    Folder.data   = [Folder.toolbox,'..\RS00',num2str(ispecimen),'\Robot\data\Condition',nside{iside},'0',num2str(icondition),'\'];
else
    Folder.data   = [Folder.toolbox,'..\RS00',num2str(ispecimen),'\Robot\data\Condition',nside{iside},num2str(icondition),'\'];
end
Trial(ntrial) = GetRobotRecords(Folder,Trial(ntrial));

% -------------------------------------------------------------------------
% GET ROBOT JOINT PLAN
% -------------------------------------------------------------------------
if iside == 1
    Folder.data = [Folder.toolbox,'..\RS00',num2str(ispecimen),'\Robot\right_plan\'];
else
    Folder.data = [Folder.toolbox,'..\RS00',num2str(ispecimen),'\Robot\left_plan\'];
end
Trial(ntrial) = GetRobotJointPlans(Folder,Trial(ntrial));

% -------------------------------------------------------------------------
% GET ROBOT SEGMENT(9) PLAN BASED ON MOCAP
% -------------------------------------------------------------------------
Folder.data   = [Folder.toolbox,'..\RS00',num2str(ispecimen),'\Matlab\'];
Trial(ntrial) = GetRobotSegmentPlans(Folder,Trial(ntrial));

cd(Folder.export);
ntrial = ntrial+1;

end % idirection
end % imotion
end % icondition
end % iside
end % ispecimen

% -------------------------------------------------------------------------
% COMPUTE SEGMENT KINEMATICS
% -------------------------------------------------------------------------
for itrial = 1:size(Trial,2)
    Trial(itrial) = ComputeSegmentKinematics(Trial(itrial));
end

% -------------------------------------------------------------------------
% APPROXIMATE MOVEMENT AMPLITUDES
% -------------------------------------------------------------------------
cd(Folder.data);
load(Trial(itrial).Robot.fileMocap);

% Express humerus rotation matrix in the thorax SCS
% -------------------------------------------------------------------------
k = 1;
for i = 1:nMarker
    O6(i,:)  = Marker.SJN(i,:);
    tY6(i,:) = (Marker.SJN(i,:)+Marker.CV7(i,:))/2 - ...
               (Marker.SXS(i,:)+Marker.TV8(i,:))/2;
    Y6(i,:)  = tY6(i,:)/norm(tY6(i,:));
    tZ6(i,:) = cross((Marker.SXS(i,:)-Marker.TV8(i,:)), ...
                (Marker.CV7(i,:)-Marker.TV8(i,:)));
    Z6(i,:)  = tZ6(i,:)/norm(tZ6(i,:));
    X6(i,:)  = cross(Y6(i,:),Z6(i,:));
end

% % FE
% vec1 = Z_flange(iframe_FE(1):iframe_FE(end),:);
% vec2 = Y6(iframe_FE(1):iframe_FE(end),:);
% for i = 1:size(vec1,1)
%     angle(i) = rad2deg(atan2(norm(cross(vec1(i,:),vec2(i,:))),dot(vec1(i,:),vec2(i,:))));
% end
% plot(angle);
% min(angle)
% max(angle)

% % AA
% vec1 = Z_flange(iframe_AA(1):iframe_AA(end),:);
% vec2 = Y6(iframe_AA(1):iframe_AA(end),:);
% for i = 1:size(vec1,1)
%     angle(i) = rad2deg(atan2(norm(cross(vec1(i,:),vec2(i,:))),dot(vec1(i,:),vec2(i,:))));
% end
% plot(angle);
% min(angle)
% max(angle)

% % IER
% vec1 = X_flange(iframe_IER(1):iframe_IER(end),:);
% vec1 = [vec1(:,1) vec1(:,2) zeros(size(vec1,1),1)];
% vec2 = X6(iframe_IER(1):iframe_IER(end),:);
% for i = 1:size(vec1,1)
%     angle(i) = rad2deg(atan2(norm(cross(vec1(i,:),vec2(i,:))),dot(vec1(i,:),vec2(i,:))));
% end
% plot((90-angle));
% min((90-angle))
% max((90-angle))

% % HFE
% vec1 = Z_flange(iframe_HFE(1):iframe_HFE(end),:);
% vec1 = [vec1(:,1) vec1(:,2) zeros(size(vec1,1),1)];
% vec2 = Z6(iframe_HFE(1):iframe_HFE(end),:);
% for i = 1:size(vec1,1)
%     angle(i) = rad2deg(atan2(norm(cross(vec1(i,:),vec2(i,:))),dot(vec1(i,:),vec2(i,:))));
% end
% plot(-(90-angle));
% min(-(90-angle))
% max(-(90-angle))

% % VT
% sqrt((Marker.EXT01(iframe_VT(1),1)-Marker.EXT01(iframe_VT(end),1))^2 + ...
%      (Marker.EXT01(iframe_VT(1),2)-Marker.EXT01(iframe_VT(end),2))^2 + ...
%      (Marker.EXT01(iframe_VT(1),3)-Marker.EXT01(iframe_VT(end),3))^2)

% % HC
% sqrt((Marker.EXT01(iframe_HC(1),1)-Marker.EXT01(iframe_HC(end),1))^2 + ...
%      (Marker.EXT01(iframe_HC(1),2)-Marker.EXT01(iframe_HC(end),2))^2 + ...
%      (Marker.EXT01(iframe_HC(1),3)-Marker.EXT01(iframe_HC(end),3))^2)

% -------------------------------------------------------------------------
% STORE EXTRACTED DATA IN A MAT FILE
% -------------------------------------------------------------------------
% MUST BE SAVED MANUALLY
% cd(Folder.export);
% save([Trial(1).specimen,'_Records'],'Trial');

% -------------------------------------------------------------------------
% QUALITY OF DATA
% -------------------------------------------------------------------------
Quality = [];
tic;
for ispecimen = 3
    for iside = 1:size(nside,2)
        for imotion = 1:size(nmotion,2)
            for idirection = 1:size(ndirection,2)
                for ijoint = 1:7
                    Quality = AssessDataQuality(Quality,Trial,ijoint,[],nspecimen,ispecimen,nside,iside,nmotion,imotion,ndirection,idirection);
                end
                for isegment = 9 % Only flange
                    Quality = AssessDataQuality(Quality,Trial,[],isegment,nspecimen,ispecimen,nside,iside,nmotion,imotion,ndirection,idirection);
                end
            end
        end
    end
end
toc
cd(Folder.export);
save([Trial(1).specimen,'_Quality'],'Quality');
% Export quality data as XLSX file
ExportDataQuality(Trial(1).specimen,Quality);

% % -------------------------------------------------------------------------
% % COMPUTE HUMERUS CLUSTER MARKERS IN ICS BASED ON ROBOT SEGMENT KINEMATICS
% % -------------------------------------------------------------------------
% Folder.data = [Folder.toolbox,'..\RS00',num2str(ispecimen),'\Mocap\robot_motions\'];
% cd(Folder.data);
% % Right humerus markers
% btkFile = 'RightStatic01_processed.c3d';
% btk = btkReadAcquisition(btkFile);
% Marker = btkGetMarkers(btk);
% humerus_mocap = [[Marker.cRHUM01 1]' [Marker.cRHUM02 1]' [Marker.cRHUM03 1]' [Marker.cRHUM04 1]'];
% flange_T_ICS = permute(mean(Trial(1).Robot.Segment(9).T.raw(1:100,:,:,1)),[2,3,1]); % First 100 frames of the first Trial used to compute mean homogeneous matrix at static initial position
% temp = flange_T_ICS*humerus_mocap;
% humerus_flange = temp(1:3,:);
% clear temp humerus_mocap;
% 
% btkFile = 'RightCondition01_processed.c3d';
% btk = btkReadAcquisition(btkFile);
% Marker = btkGetMarkers(btk);
% Event = btkGetEvents(btk);
% humerus_mocap = [permute(Marker.cRHUM01,[2,3,1]) ...
%                  permute(Marker.cRHUM02,[2,3,1]) ...
%                  permute(Marker.cRHUM03,[2,3,1]) ...
%                  permute(Marker.cRHUM04,[2,3,1])];
% for i = 1:size(permute(Trial(1).Robot.Segment(9).T.cycle(:,:,:,1),[2,3,1]),3)
%     temp(:,:,i) = permute(Trial(1).Robot.Segment(9).T.cycle(i,:,:,1),[2,3,1]).*[humerus_flange;[1 1 1 1]];
% end
% humerus_robot = temp(1:3,:,:);
% figure();
% hold on;
% plot(interpft(permute(humerus_mocap(:,1,2033:7261),[3,1,2]),101),'red');
% plot(permute(humerus_robot(:,1,:),[3,1,2]),'blue');
% 
% PlotRobotMotion(Trial(1));

% Traw give the position and orientation of the terminal element of the
% robot (flange) express in the robot coordinate system
% The static file of the moca records give the 3d position of the markers
% of the humerus in the robot coordinate system
% Using the first frames, it is thus possible to compute the position of
% the markers in the flange coordinate system and then to recompute them at
% each frame of movement of the flange in the robot coordinate system
% Make a plot to check !
% Objective = validation of the mocap 3d positioning
% Metric = DTW ?
% Reliability is also possible across repetitions of a same condition and 
% across conditions (DTW again)




%%
%       - DECOUPER LE CODE EN FONCTIONS
% - ORGANISER LE REPERTOIRE DE LA TOOLBOX POUR MEILLEURE IDENTIFICATION DES
%   CODES ROBOTS OU MARKERS + CODE MATLAB OU ROS
% - RENDRE GENERIQUE MON CODE DE TRAITEMENT DES MARQUEURS POUR QU'IL SOIT CROSS-PROJETS
% - FAIRE UN MAIN_COLLECTION ET UN MAIN_PROCESSING DANS LA TOOLBOX

% - Gestion multi trial comme pour Markers? = charger tous les fichiers d'un coup
% - Gestion multi motion comme pour Markers? = charger tous les fichiers d'un coup
% - Association des Trial/Axis et Trial/Marker ? manuel? ordre? horaire?

%       - Calculer la r�p�tabililt� sur les robot joint angle
%       - Calculer la r�p�tabililt� sur position/orientation �l�ment
%         terminal / humerus
%       - Calculer l'erreur sur position/orientation �l�ment terminal / consigne mocap
%           = calculer la flange avec les joint plut�t que cartesian pose qui n'est 
%             pas bien �chantillonn�
% - Calculer erreur de mesure mocap = DEFINIR UN TRIAL.MOTION = 'static' pour 
%   d�finir la position du cluster humerus dans le rep�re de la flange 
%   = calculer la flange avec les joint plut�t que cartesian pose qui n'est 
%     pas bien �chantillonn�
% - Calculer stabilit� du tronc (r�p�tabilit� mvt tronc) avec mvt cluster
%   tronc


