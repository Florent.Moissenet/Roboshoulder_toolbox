% Author     :   F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License    :   Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code:   https://github.com/fmoissenet/Roboshoulder_Toolbox
% Reference  :   To be defined
% Date       :   March 2020
% -------------------------------------------------------------------------
% Description:   This routine aims to get the bones geometry and related
%                pose/kinematics from CTscan derived STL images and mocap 
%                marker trajectories, respectively. 
%                It is part of the toolkit grouping several methods used in 
%                the RoboShoulder project, a joined project with the HEPIA 
%                school at Geneva.
% -------------------------------------------------------------------------
% Dependencies : - SphereFit: http://www.mathworks.com/matlabcentral/fileexchange/22643
%                - Toolbox Kinematics Inverse Dynamics: https://fr.mathworks.com/matlabcentral/fileexchange/58021-3d-kinematics-and-inverse-dynamics
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% INIT THE WORKSPACE
% -------------------------------------------------------------------------
clearvars;
close all;
warning off;
clc;

% -------------------------------------------------------------------------
% SET FOLDERS
% -------------------------------------------------------------------------
Folder.toolbox      = 'C:\Users\moissene\OneDrive - unige.ch\2019 - ROBOSHOULDER\Data\Roboshoulder_toolbox\';
Folder.data         = 'C:\Users\moissene\OneDrive - unige.ch\2019 - ROBOSHOULDER\Data\RS001\';
Folder.export       = 'C:\Users\moissene\OneDrive - unige.ch\2019 - ROBOSHOULDER\Data\RS001\Matlab\';
Folder.dependencies = [Folder.toolbox,'dependencies\']; % these binaries are prepared for a recent version of Matlab and Windows 8 or newer
addpath(Folder.toolbox);
addpath(genpath(Folder.dependencies));

% -------------------------------------------------------------------------
% SET SUBJECT INFO
% -------------------------------------------------------------------------
Subject.id   = 'RS001';
Subject.side = 'R'; % R or L
disp(['Subject ID: ',Subject.id]);
disp(['Assessed side: ',Subject.side]);

%% ------------------------------------------------------------------------
% GET BONES GEOMETRY
% -------------------------------------------------------------------------

% Initialise plot
figure(); hold on; axis equal; legend off; camlight headlight; lighting gouraud;

% HUMERUS
% -------------------------------------------------------------------------
disp('Load humerus geometry');

% Get the bone geometry from STL file
% -------------------------------------------------------------------------
cd(Folder.data);
cd('Imagerie\Scan\STL\');
stlFile                      = [Subject.id,'_',Subject.side,'_Mesh_Humerus_cluster_light.stl'];
Bone(1).label                = 'RHumerus';
temp                         = stlread(stlFile);
Bone(1).Static.Mesh.vertices = permute(temp.Points*1e-3,[2,1,3]); % m
Bone(1).Static.Mesh.faces    = permute(temp.ConnectivityList,[2,1,3]);
% Plot
patch_array3(Bone(1).Static.Mesh.faces,...
             Bone(1).Static.Mesh.vertices,...
             [0.7 0.7 0.7],'none','gouraud',1);
xlabel('x'); ylabel('y'); zlabel('z');
light('position',[1.5 1.5 0.75],'Style','infinite');
% Clear workspace
clear stlFile temp;
  
% Get the cluster markers by sphere fitting (cRHUM0i)
% Use points placed on the markers surface (TXT file)
% -------------------------------------------------------------------------
cd(Folder.data);
cd('Imagerie\Scan\NMS\');
% cRHUM01
txtFile                                = 'RS001_R_Humerus_cRHUM01.txt';
pointCloud                             = csvread(txtFile,0,1)*1e-3; % m
Bone(1).Static.Landmark(1).label       = 'cRHUM01';
Bone(1).Static.Landmark(1).coordinates = sphereFit(pointCloud)';
clear txtFile pointCloud;
% cRHUM02
txtFile                                = 'RS001_R_Humerus_cRHUM02.txt';
pointCloud                             = csvread(txtFile,0,1)*1e-3; % m
Bone(1).Static.Landmark(2).label       = 'cRHUM02';
Bone(1).Static.Landmark(2).coordinates = sphereFit(pointCloud)';
clear txtFile pointCloud;
% cRHUM03
txtFile                                = 'RS001_R_Humerus_cRHUM03.txt';
pointCloud                             = csvread(txtFile,0,1)*1e-3; % m
Bone(1).Static.Landmark(3).label       = 'cRHUM03';
Bone(1).Static.Landmark(3).coordinates = sphereFit(pointCloud)';
clear txtFile pointCloud;
% cRHUM04
txtFile                                = 'RS001_R_Humerus_cRHUM04.txt';
pointCloud                             = csvread(txtFile,0,1)*1e-3; % m
Bone(1).Static.Landmark(4).label       = 'cRHUM04';
Bone(1).Static.Landmark(4).coordinates = sphereFit(pointCloud)';
% Plot
for imarker = 1:4
    plot3(Bone(1).Static.Landmark(imarker).coordinates(1,1,1),...
          Bone(1).Static.Landmark(imarker).coordinates(2,1,1),...
          Bone(1).Static.Landmark(imarker).coordinates(3,1,1),...
          'Marker','o','MarkerSize',15,'Color','red');
end
% Clear workspace
clear txtFile pointCloud imarker;

% Get bony landmarks
% Use points defined on bone mesh (TXT file)
% -------------------------------------------------------------------------
cd(Folder.data);
cd('Imagerie\Scan\NMS\');
txtFile    = 'RS001_R_Humerus_Landmarks.txt';
pointCloud = csvread(txtFile,0,1)*1e-3; % m
% RHLE
Bone(1).Static.Landmark(5).label       = 'RHLE';
Bone(1).Static.Landmark(5).coordinates = pointCloud(1,:)';
% RHME
Bone(1).Static.Landmark(6).label       = 'RHME';
Bone(1).Static.Landmark(6).coordinates = pointCloud(2,:)';
% RHMU
Bone(1).Static.Landmark(7).label       = 'RHMU';
Bone(1).Static.Landmark(7).coordinates = pointCloud(3,:)';
% RHML
Bone(1).Static.Landmark(8).label       = 'RHML';
Bone(1).Static.Landmark(8).coordinates = pointCloud(4,:)';
% RHGT
Bone(1).Static.Landmark(9).label       = 'RHGT';
Bone(1).Static.Landmark(9).coordinates = pointCloud(5,:)';
% RHLT
Bone(1).Static.Landmark(10).label       = 'RHLT';
Bone(1).Static.Landmark(10).coordinates = pointCloud(6,:)';
% RHDT
Bone(1).Static.Landmark(11).label       = 'RHDT';
Bone(1).Static.Landmark(11).coordinates = pointCloud(7,:)';
% Plot
for imarker = 5:11
    plot3(Bone(1).Static.Landmark(imarker).coordinates(1,1,1),...
          Bone(1).Static.Landmark(imarker).coordinates(2,1,1),...
          Bone(1).Static.Landmark(imarker).coordinates(3,1,1),...
          'Marker','.','MarkerSize',15,'Color','green');
end
% Clear workspace
clear txtFile pointCloud imarker;

% Set the humeral head centre by sphere fitting
% Use points placed on the humeral head (TXT file)
% -------------------------------------------------------------------------
cd(Folder.data);
cd('Imagerie\Scan\NMS\');
txtFile                                 = 'RS001_R_Humerus_Humeral_head.txt';
pointCloud                              = csvread(txtFile,0,1)*1e-3; % m
Bone(1).Static.Object(1).label          = 'Humeral head sphere';
[Bone(1).Static.Object(1).centre, ...
 Bone(1).Static.Object(1).radius]       = sphereFit(pointCloud);
Bone(1).Static.Object(1).centre         = Bone(1).Static.Object(1).centre';
Bone(1).Static.Landmark(12).label       = 'Humeral head centre';
Bone(1).Static.Landmark(12).coordinates = Bone(1).Static.Object(1).centre;
% Plot
[x,y,z] = sphere;
surf(x*Bone(1).Static.Object(1).radius+Bone(1).Static.Object(1).centre(1,1,1), ...
     y*Bone(1).Static.Object(1).radius+Bone(1).Static.Object(1).centre(2,1,1), ...
     z*Bone(1).Static.Object(1).radius+Bone(1).Static.Object(1).centre(3,1,1),...
     'Facecolor','blue','FaceAlpha',0.3);
% Clear workspace
clear txtFile pointCloud x y z;

% SCAPULA
% -------------------------------------------------------------------------
disp('Load scapula geometry');

% Get the bone geometry from STL file
% -------------------------------------------------------------------------
cd(Folder.data);
cd('Imagerie\Scan\STL\');
stlFile                      = [Subject.id,'_',Subject.side,'_Mesh_Scapula_cluster_light.stl'];
Bone(2).label                = 'RScapula';
temp                         = stlread(stlFile);
Bone(2).Static.Mesh.vertices = permute(temp.Points*1e-3,[2,1,3]); % m
Bone(2).Static.Mesh.faces    = permute(temp.ConnectivityList,[2,1,3]);
% Plot
patch_array3(Bone(2).Static.Mesh.faces,...
             Bone(2).Static.Mesh.vertices,...
             [0.7 0.7 0.7],'none','gouraud',1);
xlabel('x'); ylabel('y'); zlabel('z');
light('position',[1.5 1.5 0.75],'Style','infinite');
% Clear workspace
clear stlFile temp;
  
% Get the cluster markers by sphere fitting (cRSCA0i)
% Use points placed on the markers surface (TXT file)
% -------------------------------------------------------------------------
cd(Folder.data);
cd('Imagerie\Scan\NMS\');
% cRHUM01
txtFile                                = 'RS001_R_Scapula_cRSCA01.txt';
pointCloud                             = csvread(txtFile,0,1)*1e-3; % m
Bone(2).Static.Landmark(1).label       = 'cRSCA01';
Bone(2).Static.Landmark(1).coordinates = sphereFit(pointCloud)';
clear txtFile pointCloud;
% cRHUM02
txtFile                                = 'RS001_R_Scapula_cRSCA02.txt';
pointCloud                             = csvread(txtFile,0,1)*1e-3; % m
Bone(2).Static.Landmark(2).label       = 'cRSCA02';
Bone(2).Static.Landmark(2).coordinates = sphereFit(pointCloud)';
clear txtFile pointCloud;
% cRHUM03
txtFile                                = 'RS001_R_Scapula_cRSCA03.txt';
pointCloud                             = csvread(txtFile,0,1)*1e-3; % m
Bone(2).Static.Landmark(3).label       = 'cRSCA03';
Bone(2).Static.Landmark(3).coordinates = sphereFit(pointCloud)';
clear txtFile pointCloud;
% cRHUM04
txtFile                                = 'RS001_R_Scapula_cRSCA04.txt';
pointCloud                             = csvread(txtFile,0,1)*1e-3; % m
Bone(2).Static.Landmark(4).label       = 'cRSCA04';
Bone(2).Static.Landmark(4).coordinates = sphereFit(pointCloud)';
% Plot
for imarker = 1:4
    plot3(Bone(2).Static.Landmark(imarker).coordinates(1,1,1),...
          Bone(2).Static.Landmark(imarker).coordinates(2,1,1),...
          Bone(2).Static.Landmark(imarker).coordinates(3,1,1),...
          'Marker','o','MarkerSize',15,'Color','red');
end
% Clear workspace
clear txtFile pointCloud imarker;

% Get bony landmarks
% Use points defined on bone mesh (TXT file)
% -------------------------------------------------------------------------
cd(Folder.data);
cd('Imagerie\Scan\NMS\');
txtFile                                = 'RS001_R_Scapula_Landmarks.txt';
pointCloud                             = csvread(txtFile,0,1)*1e-3; % m
% RSIA
Bone(2).Static.Landmark(5).label       = 'RSIA';
Bone(2).Static.Landmark(5).coordinates = pointCloud(1,:)';
% RSSA
Bone(2).Static.Landmark(6).label       = 'RSSA';
Bone(2).Static.Landmark(6).coordinates = pointCloud(2,:)';
% RSAA
Bone(2).Static.Landmark(7).label       = 'RSAA';
Bone(2).Static.Landmark(7).coordinates = pointCloud(3,:)';
% RSAT
Bone(2).Static.Landmark(8).label       = 'RSAT';
Bone(2).Static.Landmark(8).coordinates = pointCloud(4,:)';
% RSAE
Bone(2).Static.Landmark(9).label       = 'RSAE';
Bone(2).Static.Landmark(9).coordinates = pointCloud(5,:)';
% RSCT
Bone(2).Static.Landmark(10).label       = 'RSCT';
Bone(2).Static.Landmark(10).coordinates = pointCloud(6,:)';
% RSAJ
Bone(2).Static.Landmark(11).label       = 'RSAJ';
Bone(2).Static.Landmark(11).coordinates = pointCloud(7,:)';
% RSRS
Bone(2).Static.Landmark(12).label       = 'RSRS';
Bone(2).Static.Landmark(12).coordinates = pointCloud(8,:)';
% Plot
for imarker = 5:12
    plot3(Bone(2).Static.Landmark(imarker).coordinates(1,1,1),...
          Bone(2).Static.Landmark(imarker).coordinates(2,1,1),...
          Bone(2).Static.Landmark(imarker).coordinates(3,1,1),...
          'Marker','.','MarkerSize',15,'Color','green');
end
% Clear workspace
clear txtFile pointCloud imarker;

% Get the glenoid contour points
% Use points defined on bone mesh (TXT file)
% -------------------------------------------------------------------------
cd(Folder.data);
cd('Imagerie\Scan\NMS\');
txtFile    = 'RS001_R_Scapula_Glenoid.txt';
pointCloud = csvread(txtFile,0,1)*1e-3; % m
nlandmark  = size(Bone(2).Static.Landmark,2);
for ipoint = 1:size(pointCloud,1)
    nlandmark = nlandmark+1;
    Bone(2).Static.Landmark(nlandmark).label       = ['Glenoid point ',num2str(ipoint)];
    Bone(2).Static.Landmark(nlandmark).coordinates = pointCloud(ipoint,:)';
end
% Set normals
contour = 1:22; % Landmarks defining the glenoid contour only
A       = [ones(size(pointCloud(contour,1))),pointCloud(contour,1),pointCloud(contour,2)]; % Solve X(3) = b(1) + b(2)*X(1) + b(3)*X(2): A = [1 X(1) X(2)]
b       = A\pointCloud(contour,3); % Solve X(3) = b(1) + b(2)*X(1) + b(3)*X(2): b = A\X(3), i.e. least square solution
b       = -b./sqrt(sum(b.^2,1));
Bone(2).Static.Object(1).label  = 'Glenoid contour normal';
Bone(2).Static.Object(1).normal = [b(2);b(3);b(1)];
% Plot
for ilandmark = 12+1:12+size(pointCloud,1)
    plot3(Bone(2).Static.Landmark(ilandmark).coordinates(1,1,1),...
          Bone(2).Static.Landmark(ilandmark).coordinates(2,1,1),...
          Bone(2).Static.Landmark(ilandmark).coordinates(3,1,1),...
          'Marker','.','MarkerSize',15,'Color','blue');
    quiver3(Bone(2).Static.Landmark(ilandmark).coordinates(1,1,1),...
            Bone(2).Static.Landmark(ilandmark).coordinates(2,1,1),...
            Bone(2).Static.Landmark(ilandmark).coordinates(3,1,1),...
            Bone(2).Static.Object(1).normal(1,1,1),...
            Bone(2).Static.Object(1).normal(2,1,1),...
            Bone(2).Static.Object(1).normal(3,1,1),...
            0.02,'Color','black');
end
% Clear workspace
clear A b txtFile pointCloud ilandmark nlandmark imarker ipoint;

% CLAVICLE
% -------------------------------------------------------------------------
disp('Load clavicle geometry');

% Get the bone geometry from STL file
% -------------------------------------------------------------------------
cd(Folder.data);
cd('Imagerie\Scan\STL\');
stlFile                      = [Subject.id,'_',Subject.side,'_Mesh_Clavicle_cluster_light.stl'];
Bone(3).label                = 'RClavicle';
temp                         = stlread(stlFile);
Bone(3).Static.Mesh.vertices = permute(temp.Points*1e-3,[2,1,3]); % m
Bone(3).Static.Mesh.faces    = permute(temp.ConnectivityList,[2,1,3]);
% Plot
patch_array3(Bone(3).Static.Mesh.faces,...
             Bone(3).Static.Mesh.vertices,...
             [0.7 0.7 0.7],'none','gouraud',1);
xlabel('x'); ylabel('y'); zlabel('z');
light('position',[1.5 1.5 0.75],'Style','infinite');
% Clear workspace
clear stlFile temp;
  
% Get the cluster markers by sphere fitting (cRCLA0i)
% Use points placed on the markers surface (TXT file)
% -------------------------------------------------------------------------
cd(Folder.data);
cd('Imagerie\Scan\NMS\');
% cRCLA01
txtFile                                = 'RS001_R_Clavicle_cRCLA01.txt';
pointCloud                             = csvread(txtFile,0,1)*1e-3; % m
Bone(3).Static.Landmark(1).label       = 'cRHUM01';
Bone(3).Static.Landmark(1).coordinates = sphereFit(pointCloud)';
clear txtFile pointCloud;
% cRCLA02
txtFile                                = 'RS001_R_Clavicle_cRCLA02.txt';
pointCloud                             = csvread(txtFile,0,1)*1e-3; % m
Bone(3).Static.Landmark(2).label       = 'cRHUM02';
Bone(3).Static.Landmark(2).coordinates = sphereFit(pointCloud)';
clear txtFile pointCloud;
% cRCLA03
txtFile                                = 'RS001_R_Clavicle_cRCLA03.txt';
pointCloud                             = csvread(txtFile,0,1)*1e-3; % m
Bone(3).Static.Landmark(3).label       = 'cRHUM03';
Bone(3).Static.Landmark(3).coordinates = sphereFit(pointCloud)';
clear txtFile pointCloud;
% cRCLA04
txtFile                                = 'RS001_R_Clavicle_cRCLA04.txt';
pointCloud                             = csvread(txtFile,0,1)*1e-3; % m
Bone(3).Static.Landmark(4).label       = 'cRHUM04';
Bone(3).Static.Landmark(4).coordinates = sphereFit(pointCloud)';
% Plot
for imarker = 1:4
    plot3(Bone(3).Static.Landmark(imarker).coordinates(1,1,1),...
          Bone(3).Static.Landmark(imarker).coordinates(2,1,1),...
          Bone(3).Static.Landmark(imarker).coordinates(3,1,1),...
          'Marker','o','MarkerSize',15,'Color','red');
end
% Clear workspace
clear txtFile pointCloud imarker;

% Get bony landmarks
% Use points defined on bone mesh (TXT file)
% -------------------------------------------------------------------------
cd(Folder.data);
cd('Imagerie\Scan\NMS\');
txtFile    = 'RS001_R_Clavicle_Landmarks.txt';
pointCloud = csvread(txtFile,0,1)*1e-3; % m
% RCAE
Bone(3).Static.Landmark(5).label       = 'RCAE';
Bone(3).Static.Landmark(5).coordinates = pointCloud(1,:)';
% RCAA
Bone(3).Static.Landmark(6).label       = 'RCAA';
Bone(3).Static.Landmark(6).coordinates = pointCloud(2,:)';
% RCAS
Bone(3).Static.Landmark(7).label       = 'RCAS';
Bone(3).Static.Landmark(7).coordinates = pointCloud(3,:)';
% RCSJ
Bone(3).Static.Landmark(8).label       = 'RCSJ';
Bone(3).Static.Landmark(8).coordinates = pointCloud(4,:)';
% RCAJ
Bone(3).Static.Landmark(9).label       = 'RCAJ';
Bone(3).Static.Landmark(9).coordinates = pointCloud(5,:)';
% Plot
for imarker = 5:9
    plot3(Bone(3).Static.Landmark(imarker).coordinates(1,1,1),...
          Bone(3).Static.Landmark(imarker).coordinates(2,1,1),...
          Bone(3).Static.Landmark(imarker).coordinates(3,1,1),...
          'Marker','.','MarkerSize',15,'Color','green');
end
% Clear workspace
clear txtFile pointCloud imarker;

% THORAX
% -------------------------------------------------------------------------
disp('Load thorax geometry');

% Get the bone geometry from STL file
% -------------------------------------------------------------------------
cd(Folder.data);
cd('Imagerie\Scan\STL\');
stlFile                      = [Subject.id,'_B_Mesh_Thorax_cluster_light.stl'];
Bone(4).label                = 'Thorax';
temp                         = stlread(stlFile);
Bone(4).Static.Mesh.vertices = permute(temp.Points*1e-3,[2,1,3]); % m
Bone(4).Static.Mesh.faces    = permute(temp.ConnectivityList,[2,1,3]);
% Plot
patch_array3(Bone(4).Static.Mesh.faces,...
             Bone(4).Static.Mesh.vertices,...
             [0.7 0.7 0.7],'none','gouraud',1);
xlabel('x'); ylabel('y'); zlabel('z');
light('position',[1.5 1.5 0.75],'Style','infinite');
% Clear workspace
clear stlFile temp;
  
% Get the cluster markers by sphere fitting (cTHO0i)
% Use points placed on the markers surface (TXT file)
% -------------------------------------------------------------------------
cd(Folder.data);
cd('Imagerie\Scan\NMS\');
% cTHO01
txtFile                                = 'RS001_B_Thorax_cTHO01.txt';
pointCloud                             = csvread(txtFile,0,1)*1e-3; % m
Bone(4).Static.Landmark(1).label       = 'cTHO01';
Bone(4).Static.Landmark(1).coordinates = sphereFit(pointCloud)';
clear txtFile pointCloud;
% cTHO02
txtFile                                = 'RS001_B_Thorax_cTHO02.txt';
pointCloud                             = csvread(txtFile,0,1)*1e-3; % m
Bone(4).Static.Landmark(2).label       = 'cTHO02';
Bone(4).Static.Landmark(2).coordinates = sphereFit(pointCloud)';
clear txtFile pointCloud;
% cTHO03
txtFile                                = 'RS001_B_Thorax_cTHO03.txt';
pointCloud                             = csvread(txtFile,0,1)*1e-3; % m
Bone(4).Static.Landmark(3).label       = 'cTHO03';
Bone(4).Static.Landmark(3).coordinates = sphereFit(pointCloud)';
clear txtFile pointCloud;
% cTHO04
txtFile                                = 'RS001_B_Thorax_cTHO04.txt';
pointCloud                             = csvread(txtFile,0,1)*1e-3; % m
Bone(4).Static.Landmark(4).label       = 'cTHO04';
Bone(4).Static.Landmark(4).coordinates = sphereFit(pointCloud)';
% Plot
for imarker = 1:4
    plot3(Bone(4).Static.Landmark(imarker).coordinates(1,1,1),...
          Bone(4).Static.Landmark(imarker).coordinates(2,1,1),...
          Bone(4).Static.Landmark(imarker).coordinates(3,1,1),...
          'Marker','o','MarkerSize',15,'Color','red');
end
% Clear workspace
clear txtFile pointCloud imarker;

% Get bony landmarks
% Use points defined on bone mesh (TXT file)
% -------------------------------------------------------------------------
cd(Folder.data);
cd('Imagerie\Scan\NMS\');
txtFile    = 'RS001_B_Thorax_Landmarks.txt';
pointCloud = csvread(txtFile,0,1)*1e-3; % m
% SJN
Bone(4).Static.Landmark(5).label       = 'SJN';
Bone(4).Static.Landmark(5).coordinates = pointCloud(1,:)';
% RSCS
Bone(4).Static.Landmark(6).label       = 'RSCS';
Bone(4).Static.Landmark(6).coordinates = pointCloud(2,:)';
% LSCS
Bone(4).Static.Landmark(7).label       = 'LSCS';
Bone(4).Static.Landmark(7).coordinates = pointCloud(3,:)';
% SME
Bone(4).Static.Landmark(8).label       = 'SME';
Bone(4).Static.Landmark(8).coordinates = pointCloud(4,:)';
% Plot
for imarker = 5:8
    plot3(Bone(4).Static.Landmark(imarker).coordinates(1,1,1),...
          Bone(4).Static.Landmark(imarker).coordinates(2,1,1),...
          Bone(4).Static.Landmark(imarker).coordinates(3,1,1),...
          'Marker','.','MarkerSize',15,'Color','green');
end
% Clear workspace
clear txtFile pointCloud imarker;
  
% Get additional landmarks by sphere fitting
% Use points placed on the markers surface (TXT file)
% -------------------------------------------------------------------------
cd(Folder.data);
cd('Imagerie\Scan\NMS\');
% CJC
txtFile                                = 'RS001_B_Thorax_CJC.txt';
pointCloud                             = csvread(txtFile,0,1)*1e-3; % m
Bone(4).Static.Landmark(9).label       = 'CJC';
Bone(4).Static.Landmark(9).coordinates = sphereFit(pointCloud)';
clear txtFile pointCloud;
% TJC
txtFile                                 = 'RS001_B_Thorax_TJC.txt';
pointCloud                              = csvread(txtFile,0,1)*1e-3; % m
Bone(4).Static.Landmark(10).label       = 'TJC';
Bone(4).Static.Landmark(10).coordinates = sphereFit(pointCloud)';
clear txtFile pointCloud;
% CV7
txtFile                                 = 'RS001_B_Thorax_CV7.txt';
pointCloud                              = csvread(txtFile,0,1)*1e-3; % m
Bone(4).Static.Landmark(11).label       = 'CV7';
Bone(4).Static.Landmark(11).coordinates = sphereFit(pointCloud)';
clear txtFile pointCloud;
% TV8
txtFile                                 = 'RS001_B_Thorax_TV8.txt';
pointCloud                              = csvread(txtFile,0,1)*1e-3; % m
Bone(4).Static.Landmark(12).label       = 'TV8';
Bone(4).Static.Landmark(12).coordinates = sphereFit(pointCloud)';
clear txtFile pointCloud;
% TV12
txtFile                                 = 'RS001_B_Thorax_TV12.txt';
pointCloud                              = csvread(txtFile,0,1)*1e-3; % m
Bone(4).Static.Landmark(13).label       = 'TV12';
Bone(4).Static.Landmark(13).coordinates = sphereFit(pointCloud)';
clear txtFile pointCloud;
% SXS
txtFile                                 = 'RS001_B_Thorax_SXS.txt';
pointCloud                              = csvread(txtFile,0,1)*1e-3; % m
Bone(4).Static.Landmark(14).label       = 'SXS';
Bone(4).Static.Landmark(14).coordinates = sphereFit(pointCloud)';
clear txtFile pointCloud;
% Plot
for imarker = 9:14
    plot3(Bone(4).Static.Landmark(imarker).coordinates(1,1,1),...
          Bone(4).Static.Landmark(imarker).coordinates(2,1,1),...
          Bone(4).Static.Landmark(imarker).coordinates(3,1,1),...
          'Marker','o','MarkerSize',15,'Color','green');
end
% Clear workspace
clear txtFile pointCloud imarker;

%% ------------------------------------------------------------------------
% APPLY MOTION TO BONES
% -------------------------------------------------------------------------
cd(Folder.data);
cd('Mocap\robot_motions\');

% Get static marker positions
% -------------------------------------------------------------------------
c3dFile              = 'RightStatic01.c3d';
btkFile              = btkReadAcquisition(c3dFile);
Marker               = btkGetMarkers(btkFile);
Static.n0            = btkGetFirstFrame(btkFile);
Static.n1            = btkGetPointFrameNumber(btkFile);
Static.f             = btkGetPointFrequency(btkFile);
timeRange            = [1;...
                        Static.n1];
Static.n0            = timeRange(1);
Static.n1            = timeRange(2);
Static               = InitialiseMarkerset(Static,Marker,timeRange);
fmethod.type         = 'intercor';
fmethod.gapThreshold = [];
smethod.type         = 'movmean';
smethod.parameter    = 50;
Static               = ProcessMarkerTrajectories([],Static,fmethod,smethod);
clear c3dFile btkFile timeRange Marker fmethod smethod;

% Get trial marker trajectories
% -------------------------------------------------------------------------
c3dFile              = 'RightCondition01.c3d';
btkFile              = btkReadAcquisition(c3dFile);
Marker               = btkGetMarkers(btkFile);
Trial.n0             = btkGetFirstFrame(btkFile);
Trial.n1             = btkGetPointFrameNumber(btkFile);
Trial.fmarker        = btkGetPointFrequency(btkFile);
Event                = btkGetEvents(btkFile);
Trial.motion         = 'AA';
timeRange            = [fix(Event.(Trial.motion)(1)*Trial.fmarker);...
                        fix(Event.(Trial.motion)(1)*Trial.fmarker)+1783-1];%fix(Event.(Trial.motion)(4)*Trial.fmarker)];
Trial.n0             = 1;
Trial.n1             = timeRange(2)-timeRange(1)+1;
Trial                = InitialiseMarkerset(Trial,Marker,timeRange);
fmethod.type         = 'intercor';
fmethod.gapThreshold = [];
smethod.type         = 'movmean';
smethod.parameter    = 50;
Trial                = ProcessMarkerTrajectories(Static,Trial,fmethod,smethod);
clear c3dFile btkFile Event timeRange Marker fmethod smethod;

% Compute bones motion
% -------------------------------------------------------------------------
disp('Compute bones motion');
for t = 1%:Trial.n1
    disp(Trial.n1-t)
    
    % HUMERUS
    % ---------------------------------------------------------------------
    % Define rigid transformation between Imaging landmark position and
    % Mocal marker position by singular value decomposition
    [R(:,:,t),d(:,:,t),rms(:,:,t)] = soder([Bone(1).Static.Landmark(1).coordinates'; ...
                                            Bone(1).Static.Landmark(2).coordinates'; ...
                                            Bone(1).Static.Landmark(3).coordinates'; ...
                                            Bone(1).Static.Landmark(4).coordinates'],...
                                           [Trial.Marker(5).Trajectory.smooth(:,:,t)'; ...
                                            Trial.Marker(6).Trajectory.smooth(:,:,t)'; ...
                                            Trial.Marker(7).Trajectory.smooth(:,:,t)'; ...
                                            Trial.Marker(8).Trajectory.smooth(:,:,t)']);
    % Landmarks
    for ilandmark = 1:size(Bone(1).Static.Landmark,2)
        Bone(1).Trial.Landmark(ilandmark).label              = Bone(1).Static.Landmark(ilandmark).label;
        Bone(1).Trial.Landmark(ilandmark).coordinates(:,:,t) = R(:,:,t)*Bone(1).Static.Landmark(ilandmark).coordinates+d(:,:,t);
    end
    % Object
    Bone(1).Trial.Object(1).label         = Bone(1).Static.Object(1).label;
    Bone(1).Trial.Object(1).centre(:,:,t) = Bone(1).Trial.Landmark(12).coordinates(:,:,t);
    Bone(1).Trial.Object(1).radius(:,:,t) = Bone(1).Static.Object(1).radius;
	% Mesh
    Bone(1).Trial.Mesh.vertices(:,:,t) = R(:,:,t)*Bone(1).Static.Mesh.vertices+repmat(d(:,:,t),[1,size(Bone(1).Static.Mesh.vertices,2)]);
    % Clear workspace
    clear R d rms ilandmark;
    
    % SCAPULA
    % ---------------------------------------------------------------------
    % Define rigid transformation between Imaging landmark position and
    % Mocal marker position by singular value decomposition
    [R(:,:,t),d(:,:,t),rms(:,:,t)] = soder([Bone(2).Static.Landmark(1).coordinates'; ...
                                            Bone(2).Static.Landmark(2).coordinates'; ...
                                            Bone(2).Static.Landmark(3).coordinates'; ...
                                            Bone(2).Static.Landmark(4).coordinates'],...
                                           [Trial.Marker(13).Trajectory.smooth(:,:,t)'; ...
                                            Trial.Marker(14).Trajectory.smooth(:,:,t)'; ...
                                            Trial.Marker(15).Trajectory.smooth(:,:,t)'; ...
                                            Trial.Marker(16).Trajectory.smooth(:,:,t)']);
    % Landmarks
    for ilandmark = 1:size(Bone(2).Static.Landmark,2)
        Bone(2).Trial.Landmark(ilandmark).label              = Bone(2).Static.Landmark(ilandmark).label;
        Bone(2).Trial.Landmark(ilandmark).coordinates(:,:,t) = R(:,:,t)*Bone(2).Static.Landmark(ilandmark).coordinates+d(:,:,t);
    end
    % Object
    Bone(2).Trial.Object(1).label         = Bone(2).Static.Object(1).label;
    Bone(2).Trial.Object(1).normal(:,:,t) = R(:,:,t)*Bone(2).Static.Object(1).normal;
	% Mesh
    Bone(2).Trial.Mesh.vertices(:,:,t) = R(:,:,t)*Bone(2).Static.Mesh.vertices+repmat(d(:,:,t),[1,size(Bone(2).Static.Mesh.vertices,2)]);
    % Clear workspace
    clear R d rms ilandmark;
    
    % Compute the shadow of the glenoid on the humerus head
    % Sphere: (X-Xs)^2+(Y-Ys)^2+(Z-Zs)^2=r^2
    % Line  : [X Y Z] = [Xl Yl Zl] + l*U
    Bone(1).Trial.Object(2).label = 'Glenoid shadow';
    jlandmark = 1;
    for ilandmark = 12+1:size(Bone(2).Trial.Landmark,2)
        r              = permute(Bone(1).Trial.Object(1).radius(:,:,t),[3,1,2]);
        Ps             = permute(Bone(1).Trial.Object(1).centre(:,:,t),[3,1,2]);
        Pl             = permute(Bone(2).Trial.Landmark(ilandmark).coordinates(:,:,t),[3,1,2]);
        U              = permute(Bone(2).Trial.Object(1).normal(:,:,t),[3,1,2]);
        fun            = @(l)((Pl(1)+l*U(1)-Ps(1))^2 + (Pl(2)+l*U(2)-Ps(2))^2 + (Pl(3)+l*U(3)-Ps(3))^2 - r^2);
        l0             = 0.001;
        option.Display = 'off';
        l              = lsqnonlin(fun,l0,0,[],option);
        Bone(1).Trial.Object(2).landmark(:,jlandmark,t) = permute(Pl+l*U,[2,3,1]);
        Bone(1).Trial.Object(2).distance(:,jlandmark,t) = l;
        jlandmark                                       = jlandmark+1;
        clear r Ps Pl U fun l0 l;
    end
    clear ilandmark jlandmark;
    
    % CLAVICLE
    % ---------------------------------------------------------------------
    % Define rigid transformation between Imaging landmark position and
    % Mocal marker position by singular value decomposition
    [R(:,:,t),d(:,:,t),rms(:,:,t)] = soder([Bone(3).Static.Landmark(1).coordinates'; ...
                                            Bone(3).Static.Landmark(2).coordinates'; ...
                                            Bone(3).Static.Landmark(3).coordinates'; ...
                                            Bone(3).Static.Landmark(4).coordinates'],...
                                           [Trial.Marker(9).Trajectory.smooth(:,:,t)'; ...
                                            Trial.Marker(10).Trajectory.smooth(:,:,t)'; ...
                                            Trial.Marker(11).Trajectory.smooth(:,:,t)'; ...
                                            Trial.Marker(12).Trajectory.smooth(:,:,t)']);
    % Landmarks
    for ilandmark = 1:size(Bone(3).Static.Landmark,2)
        Bone(3).Trial.Landmark(ilandmark).label              = Bone(3).Static.Landmark(ilandmark).label;
        Bone(3).Trial.Landmark(ilandmark).coordinates(:,:,t) = R(:,:,t)*Bone(3).Static.Landmark(ilandmark).coordinates+d(:,:,t);
    end
    % Object
	% Mesh
    Bone(3).Trial.Mesh.vertices(:,:,t) = R(:,:,t)*Bone(3).Static.Mesh.vertices+repmat(d(:,:,t),[1,size(Bone(3).Static.Mesh.vertices,2)]);
    % Clear workspace
    clear R d rms ilandmark;
    
    % THORAX
    % ---------------------------------------------------------------------
    % Define rigid transformation between Imaging landmark position and
    % Mocal marker position by singular value decomposition
    [R(:,:,t),d(:,:,t),rms(:,:,t)] = soder([Bone(4).Static.Landmark(1).coordinates'; ...
                                            Bone(4).Static.Landmark(2).coordinates'; ...
                                            Bone(4).Static.Landmark(3).coordinates'; ...
                                            Bone(4).Static.Landmark(4).coordinates'],...
                                           [Trial.Marker(1).Trajectory.smooth(:,:,t)'; ...
                                            Trial.Marker(2).Trajectory.smooth(:,:,t)'; ...
                                            Trial.Marker(3).Trajectory.smooth(:,:,t)'; ...
                                            Trial.Marker(4).Trajectory.smooth(:,:,t)']);
    % Landmarks
    for ilandmark = 1:size(Bone(4).Static.Landmark,2)
        Bone(4).Trial.Landmark(ilandmark).label              = Bone(4).Static.Landmark(ilandmark).label;
        Bone(4).Trial.Landmark(ilandmark).coordinates(:,:,t) = R(:,:,t)*Bone(4).Static.Landmark(ilandmark).coordinates+d(:,:,t);
    end
    % Object
	% Mesh
    Bone(4).Trial.Mesh.vertices(:,:,t) = R(:,:,t)*Bone(4).Static.Mesh.vertices+repmat(d(:,:,t),[1,size(Bone(4).Static.Mesh.vertices,2)]);
    % Clear workspace
    clear R d rms ilandmark;
    
end
clear t;

%% ------------------------------------------------------------------------
% DEFINE SEGMENT COORDINATE SYSTEMS
% -------------------------------------------------------------------------
% Option 1: Based on ISB recommendations (Wu et al. 2015)
% -------------------------------------------------------------------------

% Define thorax coordinate system
% Wu et al. 2005
Bone(4).Trial.SCS.O = Bone(4).Trial.Landmark(5).coordinates;
Bone(4).Trial.SCS.Y = (Bone(4).Trial.Landmark(5).coordinates + ...
                       Bone(4).Trial.Landmark(11).coordinates)/2 - ...
                      (Bone(4).Trial.Landmark(12).coordinates + ...
                       Bone(4).Trial.Landmark(14).coordinates)/2;
Bone(4).Trial.SCS.Y = Bone(4).Trial.SCS.Y./sqrt(sum(Bone(4).Trial.SCS.Y.^2,1));
Bone(4).Trial.SCS.Z = cross(Bone(4).Trial.Landmark(5).coordinates - ...
                            (Bone(4).Trial.Landmark(12).coordinates + ...
                             Bone(4).Trial.Landmark(14).coordinates)/2, ...
                            Bone(4).Trial.Landmark(11).coordinates - ...
                            (Bone(4).Trial.Landmark(12).coordinates + ...
                             Bone(4).Trial.Landmark(14).coordinates)/2);
Bone(4).Trial.SCS.Z = Bone(4).Trial.SCS.Z./sqrt(sum(Bone(4).Trial.SCS.Z.^2,1));
Bone(4).Trial.SCS.X = cross(Bone(4).Trial.SCS.Y,Bone(4).Trial.SCS.Z);
Bone(4).Trial.SCS.X = Bone(4).Trial.SCS.X./sqrt(sum(Bone(4).Trial.SCS.X.^2,1));
% Homogeneous matrix
Bone(4).Trial.SCS.T  = [Bone(4).Trial.SCS.X ...
                        Bone(4).Trial.SCS.Y ...
                        Bone(4).Trial.SCS.Z ...
                        Bone(4).Trial.SCS.O; ...
                        zeros(1,1,size(Bone(4).Trial.SCS.X,3)) ...
                        zeros(1,1,size(Bone(4).Trial.SCS.X,3)) ...
                        zeros(1,1,size(Bone(4).Trial.SCS.X,3)) ...
                        ones(1,1,size(Bone(4).Trial.SCS.X,3))];
% Related Q parameters (Naaim thesis)
rP4                  = (Bone(4).Trial.Landmark(5).coordinates + ...
                        Bone(4).Trial.Landmark(11).coordinates)/2;
rD4                  = (Bone(4).Trial.Landmark(12).coordinates + ...
                        Bone(4).Trial.Landmark(14).coordinates)/2;
w4                   = Bone(4).Trial.SCS.Z;
u4                   = cross((rP4-rD4),w4);
u4                   = u4./sqrt(sum(u4.^2,1));
Bone(4).Trial.SCS.Q  = [u4 rP4 rD4 w4];
Bone(4).Trial.SCS.rM = [Bone(4).Trial.Landmark(1) ...
                        Bone(4).Trial.Landmark(2) ...
                        Bone(4).Trial.Landmark(3) ...
                        Bone(4).Trial.Landmark(4)]; % Cluster markers

% Define clavicle coordinate system
% Wu et al. 2005
Bone(3).Trial.SCS.O = Bone(3).Trial.Landmark(7).coordinates;
Bone(3).Trial.SCS.Z = Bone(3).Trial.Landmark(9).coordinates - ...
                      Bone(3).Trial.Landmark(7).coordinates;
Bone(3).Trial.SCS.Z = Bone(3).Trial.SCS.Z./sqrt(sum(Bone(3).Trial.SCS.Z.^2,1));
Bone(3).Trial.SCS.X = cross(Bone(4).Trial.SCS.Y,Bone(3).Trial.SCS.Z);
Bone(3).Trial.SCS.X = Bone(3).Trial.SCS.X./sqrt(sum(Bone(3).Trial.SCS.X.^2,1));
Bone(3).Trial.SCS.Y = cross(Bone(3).Trial.SCS.Z,Bone(3).Trial.SCS.X);
Bone(3).Trial.SCS.Y = Bone(3).Trial.SCS.Y./sqrt(sum(Bone(3).Trial.SCS.Y.^2,1));
% Homogeneous matrix
Bone(3).Trial.SCS.T  = [Bone(3).Trial.SCS.X ...
                        Bone(3).Trial.SCS.Y ...
                        Bone(3).Trial.SCS.Z ...
                        Bone(3).Trial.SCS.O; ...
                        zeros(1,1,size(Bone(3).Trial.SCS.X,3)) ...
                        zeros(1,1,size(Bone(3).Trial.SCS.X,3)) ...
                        zeros(1,1,size(Bone(3).Trial.SCS.X,3)) ...
                        ones(1,1,size(Bone(3).Trial.SCS.X,3))];
% Related Q parameters (no reference)
u3                   = Bone(3).Trial.SCS.X;
rP3                  = Bone(3).Trial.Landmark(7).coordinates;
rD3                  = Bone(3).Trial.Landmark(9).coordinates;
w3                   = Bone(3).Trial.SCS.Z;
Bone(3).Trial.SCS.Q  = [u3 rP3 rD3 w3];
Bone(3).Trial.SCS.rM = [Bone(3).Trial.Landmark(1) ...
                        Bone(3).Trial.Landmark(2) ...
                        Bone(3).Trial.Landmark(3) ...
                        Bone(3).Trial.Landmark(4)]; % Cluster markers

% Define scapula coordinate system
% Wu et al. 2005
Bone(2).Trial.SCS.O = Bone(2).Trial.Landmark(7).coordinates;
Bone(2).Trial.SCS.Z = Bone(2).Trial.Landmark(7).coordinates - ...
                      Bone(2).Trial.Landmark(12).coordinates;
Bone(2).Trial.SCS.Z = Bone(2).Trial.SCS.Z./sqrt(sum(Bone(2).Trial.SCS.Z.^2,1));
Bone(2).Trial.SCS.X = cross((Bone(2).Trial.Landmark(12).coordinates - ...
                             Bone(2).Trial.Landmark(5).coordinates),...
                            (Bone(2).Trial.Landmark(7).coordinates - ...
                             Bone(2).Trial.Landmark(5).coordinates));
Bone(2).Trial.SCS.X = Bone(2).Trial.SCS.X./sqrt(sum(Bone(2).Trial.SCS.X.^2,1));
Bone(2).Trial.SCS.Y = cross(Bone(2).Trial.SCS.Z,Bone(2).Trial.SCS.X);
Bone(2).Trial.SCS.Y = Bone(2).Trial.SCS.Y./sqrt(sum(Bone(2).Trial.SCS.Y.^2,1));
% Homogeneous matrix
Bone(2).Trial.SCS.T  = [Bone(2).Trial.SCS.X ...
                        Bone(2).Trial.SCS.Y ...
                        Bone(2).Trial.SCS.Z ...
                        Bone(2).Trial.SCS.O; ...
                        zeros(1,1,size(Bone(2).Trial.SCS.X,3)) ...
                        zeros(1,1,size(Bone(2).Trial.SCS.X,3)) ...
                        zeros(1,1,size(Bone(2).Trial.SCS.X,3)) ...
                        ones(1,1,size(Bone(2).Trial.SCS.X,3))];
% Related Q parameters (Naaim thesis)
u2                   = Bone(2).Trial.SCS.X;
rP2                  = Bone(2).Trial.Landmark(11).coordinates;
for t = 1%:Trial.n1
    temp = [];
    for ipoint = 1:22 % Number of points defining the glenoid contour
        temp = [temp Bone(2).Trial.Landmark(12+ipoint).coordinates(:,:,t)];
    end
    rD2(:,:,t) = sum(temp,2)/22; % Centre of the glenoid fossa
end
w2                   = Bone(2).Trial.Landmark(7).coordinates - ...
                       Bone(2).Trial.Landmark(12).coordinates;
Bone(2).Trial.SCS.Q  = [u2 rP2 rD2 w2];
Bone(2).Trial.SCS.rM = [Bone(2).Trial.Landmark(1) ...
                        Bone(2).Trial.Landmark(2) ...
                        Bone(2).Trial.Landmark(3) ...
                        Bone(2).Trial.Landmark(4)]; % Cluster markers
clear temp;                    
                    
% Define humerus coordinate system
% Wu et al. 2005 (1st option)
Bone(1).Trial.SCS.O = Bone(1).Trial.Landmark(12).coordinates;
Bone(1).Trial.SCS.Y = Bone(1).Trial.Landmark(12).coordinates - ...
                      (Bone(1).Trial.Landmark(5).coordinates + ...
                       Bone(1).Trial.Landmark(6).coordinates)/2;
Bone(1).Trial.SCS.Y = Bone(1).Trial.SCS.Y./sqrt(sum(Bone(1).Trial.SCS.Y.^2,1));
Bone(1).Trial.SCS.X = cross((Bone(1).Trial.Landmark(5).coordinates - ...
                             Bone(1).Trial.Landmark(12).coordinates),...
                            (Bone(1).Trial.Landmark(6).coordinates - ...
                             Bone(1).Trial.Landmark(12).coordinates));
Bone(1).Trial.SCS.X = Bone(1).Trial.SCS.X./sqrt(sum(Bone(1).Trial.SCS.X.^2,1));
Bone(1).Trial.SCS.Z = cross(Bone(1).Trial.SCS.X,Bone(1).Trial.SCS.Y);
Bone(1).Trial.SCS.Z = Bone(1).Trial.SCS.Z./sqrt(sum(Bone(1).Trial.SCS.Z.^2,1));
% Homogeneous matrix
Bone(1).Trial.SCS.T  = [Bone(1).Trial.SCS.X ...
                        Bone(1).Trial.SCS.Y ...
                        Bone(1).Trial.SCS.Z ...
                        Bone(1).Trial.SCS.O; ...
                        zeros(1,1,size(Bone(1).Trial.SCS.X,3)) ...
                        zeros(1,1,size(Bone(1).Trial.SCS.X,3)) ...
                        zeros(1,1,size(Bone(1).Trial.SCS.X,3)) ...
                        ones(1,1,size(Bone(1).Trial.SCS.X,3))];
% Related Q parameters (Naaim thesis)
u1                   = Bone(1).Trial.SCS.X;
rP1                  = Bone(1).Trial.Landmark(12).coordinates;
rD1                  = (Bone(1).Trial.Landmark(5).coordinates + ...
                        Bone(1).Trial.Landmark(6).coordinates)/2;
w1                   = Bone(1).Trial.SCS.Z;
Bone(1).Trial.SCS.Q  = [u1 rP1 rD1 w1];
Bone(1).Trial.SCS.rM = [Bone(1).Trial.Landmark(1) ...
                        Bone(1).Trial.Landmark(2) ...
                        Bone(1).Trial.Landmark(3) ...
                        Bone(1).Trial.Landmark(4)]; % Cluster markers

%% ------------------------------------------------------------------------
% PLOT
% -------------------------------------------------------------------------
figure(2);
for t = 1%:50:Trial.n1
    % Right humerus
    mesh1 = patch_array3(Bone(1).Static.Mesh.faces(1:3,:),...
                         Bone(1).Trial.Mesh.vertices(1:3,:,t),...
                         [0.7 0.7 0.7],'none','gouraud',0.8);
    hold on; axis equal; legend off;
    for imarker = 5:8
        plot3(Trial.Marker(imarker).Trajectory.smooth(1,:,t),...
              Trial.Marker(imarker).Trajectory.smooth(2,:,t),...
              Trial.Marker(imarker).Trajectory.smooth(3,:,t),...
              'Marker','o','MarkerSize',15,'Color','red'); 
    end
    for ilandmark = 5:11
        plot3(Bone(1).Trial.Landmark(ilandmark).coordinates(1,:,t),...
              Bone(1).Trial.Landmark(ilandmark).coordinates(2,:,t),...
              Bone(1).Trial.Landmark(ilandmark).coordinates(3,:,t),...
              'Marker','.','MarkerSize',15,'Color','green'); 
    end
    X = [];
    Y = [];
    Z = [];
    for ilandmark = 1:size(Bone(1).Trial.Object(2).landmark,2)
        X = [X Bone(1).Trial.Object(2).landmark(1,ilandmark,t)];
        Y = [Y Bone(1).Trial.Object(2).landmark(2,ilandmark,t)];
        Z = [Z Bone(1).Trial.Object(2).landmark(3,ilandmark,t)];
%         if Bone(1).Trial.Object(2).distance < 1e-3
%         plot3(Bone(1).Trial.Object(2).landmark(1,ilandmark,t),...
%               Bone(1).Trial.Object(2).landmark(2,ilandmark,t),...
%               Bone(1).Trial.Object(2).landmark(i3,landmark,t),...
%               'Marker','.','MarkerSize',15,'Color','magenta'); 
    end
    T = delaunayTriangulation(X',Y',Z');
    tetramesh(T,'FaceColor',[0.3 0.3 0.3],'EdgeColor','none');
    clear X Y Z T;
    [x,y,z] = sphere;
    surf(x*Bone(1).Trial.Object(1).radius(:,:,t)+Bone(1).Trial.Object(1).centre(1,:,t), ...
         y*Bone(1).Trial.Object(1).radius(:,:,t)+Bone(1).Trial.Object(1).centre(2,:,t), ...
         z*Bone(1).Trial.Object(1).radius(:,:,t)+Bone(1).Trial.Object(1).centre(3,:,t),...
         'Facecolor','blue','FaceAlpha',0.3);
    clear x y z;
    quiver3(Bone(1).Trial.SCS.O(1,:,t),Bone(1).Trial.SCS.O(2,:,t),Bone(1).Trial.SCS.O(3,:,t), ...
            Bone(1).Trial.SCS.X(1,:,t),Bone(1).Trial.SCS.X(2,:,t),Bone(1).Trial.SCS.X(3,:,t),...
            0.1,'Color','red');
    quiver3(Bone(1).Trial.SCS.O(1,:,t),Bone(1).Trial.SCS.O(2,:,t),Bone(1).Trial.SCS.O(3,:,t), ...
            Bone(1).Trial.SCS.Y(1,:,t),Bone(1).Trial.SCS.Y(2,:,t),Bone(1).Trial.SCS.Y(3,:,t),...
            0.1,'Color','green');
    quiver3(Bone(1).Trial.SCS.O(1,:,t),Bone(1).Trial.SCS.O(2,:,t),Bone(1).Trial.SCS.O(3,:,t), ...
            Bone(1).Trial.SCS.Z(1,:,t),Bone(1).Trial.SCS.Z(2,:,t),Bone(1).Trial.SCS.Z(3,:,t),...
            0.1,'Color','blue');
    % Right scapula
    mesh2 = patch_array3(Bone(2).Static.Mesh.faces(1:3,:),...
                         Bone(2).Trial.Mesh.vertices(1:3,:,t),...
                         [0.7 0.7 0.7],'none','gouraud',0.8);
    for imarker = 13:16
        plot3(Trial.Marker(imarker).Trajectory.smooth(1,:,t),...
              Trial.Marker(imarker).Trajectory.smooth(2,:,t),...
              Trial.Marker(imarker).Trajectory.smooth(3,:,t),...
              'Marker','o','MarkerSize',15,'Color','red'); 
    end
    for ilandmark = 5:12
        plot3(Bone(2).Trial.Landmark(ilandmark).coordinates(1,:,t),...
              Bone(2).Trial.Landmark(ilandmark).coordinates(2,:,t),...
              Bone(2).Trial.Landmark(ilandmark).coordinates(3,:,t),...
              'Marker','o','MarkerSize',10,'Color','green'); 
    end
    for ilandmark = 12+1:size(Bone(2).Trial.Landmark,2)
        plot3(Bone(2).Trial.Landmark(ilandmark).coordinates(1,:,t),...
              Bone(2).Trial.Landmark(ilandmark).coordinates(2,:,t),...
              Bone(2).Trial.Landmark(ilandmark).coordinates(3,:,t),...
              'Marker','.','MarkerSize',15,'Color','blue'); 
        quiver3(Bone(2).Trial.Landmark(ilandmark).coordinates(1,:,t),...
                Bone(2).Trial.Landmark(ilandmark).coordinates(2,:,t),...
                Bone(2).Trial.Landmark(ilandmark).coordinates(3,:,t), ...
                Bone(2).Trial.Object(1).normal(1,:,t),...
                Bone(2).Trial.Object(1).normal(2,:,t),...
                Bone(2).Trial.Object(1).normal(3,:,t),...
                0.02,'Color','black');
    end
    quiver3(Bone(2).Trial.SCS.O(1,:,t),Bone(2).Trial.SCS.O(2,:,t),Bone(2).Trial.SCS.O(3,:,t), ...
            Bone(2).Trial.SCS.X(1,:,t),Bone(2).Trial.SCS.X(2,:,t),Bone(2).Trial.SCS.X(3,:,t),...
            0.1,'Color','red');
    quiver3(Bone(2).Trial.SCS.O(1,:,t),Bone(2).Trial.SCS.O(2,:,t),Bone(2).Trial.SCS.O(3,:,t), ...
            Bone(2).Trial.SCS.Y(1,:,t),Bone(2).Trial.SCS.Y(2,:,t),Bone(2).Trial.SCS.Y(3,:,t),...
            0.1,'Color','green');
    quiver3(Bone(2).Trial.SCS.O(1,:,t),Bone(2).Trial.SCS.O(2,:,t),Bone(2).Trial.SCS.O(3,:,t), ...
            Bone(2).Trial.SCS.Z(1,:,t),Bone(2).Trial.SCS.Z(2,:,t),Bone(2).Trial.SCS.Z(3,:,t),...
            0.1,'Color','blue');
    % Right clavicle
    mesh3 = patch_array3(Bone(3).Static.Mesh.faces(1:3,:),...
                         Bone(3).Trial.Mesh.vertices(1:3,:,t),...
                         [0.7 0.7 0.7],'none','gouraud',0.8);
    for imarker = 9:12
        plot3(Trial.Marker(imarker).Trajectory.smooth(1,:,t),...
              Trial.Marker(imarker).Trajectory.smooth(2,:,t),...
              Trial.Marker(imarker).Trajectory.smooth(3,:,t),...
              'Marker','o','MarkerSize',15,'Color','red'); 
    end
    for ilandmark = 5:9
        plot3(Bone(3).Trial.Landmark(ilandmark).coordinates(1,:,t),...
              Bone(3).Trial.Landmark(ilandmark).coordinates(2,:,t),...
              Bone(3).Trial.Landmark(ilandmark).coordinates(3,:,t),...
              'Marker','.','MarkerSize',15,'Color','green'); 
    end   
    quiver3(Bone(3).Trial.SCS.O(1,:,t),Bone(3).Trial.SCS.O(2,:,t),Bone(3).Trial.SCS.O(3,:,t), ...
            Bone(3).Trial.SCS.X(1,:,t),Bone(3).Trial.SCS.X(2,:,t),Bone(3).Trial.SCS.X(3,:,t),...
            0.1,'Color','red');
    quiver3(Bone(3).Trial.SCS.O(1,:,t),Bone(3).Trial.SCS.O(2,:,t),Bone(3).Trial.SCS.O(3,:,t), ...
            Bone(3).Trial.SCS.Y(1,:,t),Bone(3).Trial.SCS.Y(2,:,t),Bone(3).Trial.SCS.Y(3,:,t),...
            0.1,'Color','green');
    quiver3(Bone(3).Trial.SCS.O(1,:,t),Bone(3).Trial.SCS.O(2,:,t),Bone(3).Trial.SCS.O(3,:,t), ...
            Bone(3).Trial.SCS.Z(1,:,t),Bone(3).Trial.SCS.Z(2,:,t),Bone(3).Trial.SCS.Z(3,:,t),...
            0.1,'Color','blue');   
    % Thorax
    % Right clavicle
    mesh4 = patch_array3(Bone(4).Static.Mesh.faces(1:3,:),...
                         Bone(4).Trial.Mesh.vertices(1:3,:,t),...
                         [0.7 0.7 0.7],'none','gouraud',0.8);
    for imarker = 1:4
        plot3(Trial.Marker(imarker).Trajectory.smooth(1,:,t),...
              Trial.Marker(imarker).Trajectory.smooth(2,:,t),...
              Trial.Marker(imarker).Trajectory.smooth(3,:,t),...
              'Marker','o','MarkerSize',15,'Color','red'); 
    end
    for ilandmark = 5:8
        plot3(Bone(4).Trial.Landmark(ilandmark).coordinates(1,:,t),...
              Bone(4).Trial.Landmark(ilandmark).coordinates(2,:,t),...
              Bone(4).Trial.Landmark(ilandmark).coordinates(3,:,t),...
              'Marker','.','MarkerSize',15,'Color','green'); 
    end   
    for ilandmark = 9:14
        plot3(Bone(4).Trial.Landmark(ilandmark).coordinates(1,:,t),...
              Bone(4).Trial.Landmark(ilandmark).coordinates(2,:,t),...
              Bone(4).Trial.Landmark(ilandmark).coordinates(3,:,t),...
              'Marker','.','MarkerSize',15,'Color','green'); 
    end  
    quiver3(Bone(4).Trial.SCS.O(1,:,t),Bone(4).Trial.SCS.O(2,:,t),Bone(4).Trial.SCS.O(3,:,t), ...
            Bone(4).Trial.SCS.X(1,:,t),Bone(4).Trial.SCS.X(2,:,t),Bone(4).Trial.SCS.X(3,:,t),...
            0.1,'Color','red');
    quiver3(Bone(4).Trial.SCS.O(1,:,t),Bone(4).Trial.SCS.O(2,:,t),Bone(4).Trial.SCS.O(3,:,t), ...
            Bone(4).Trial.SCS.Y(1,:,t),Bone(4).Trial.SCS.Y(2,:,t),Bone(4).Trial.SCS.Y(3,:,t),...
            0.1,'Color','green');
    quiver3(Bone(4).Trial.SCS.O(1,:,t),Bone(4).Trial.SCS.O(2,:,t),Bone(4).Trial.SCS.O(3,:,t), ...
            Bone(4).Trial.SCS.Z(1,:,t),Bone(4).Trial.SCS.Z(2,:,t),Bone(4).Trial.SCS.Z(3,:,t),...
            0.1,'Color','blue');  
    % Settings    
    view(0,90)
    lightangle(gca,-45,30);
    xlim([0.4 0.9]);
    ylim([0.3 0.9]);
    zlim([-0.2 0.2]);
%     pause(0.1);
%     delete(mesh1);
%     delete(mesh2);
%     delete(mesh3);
%     delete(mesh4);
%     clf;
%     hold off;
end
clear t mesh1 mesh2 mesh3 ilandmark imarker;

%% ------------------------------------------------------------------------
% COMPUTE KINEMATICS
% -------------------------------------------------------------------------

n = Trial.n1;

% Humerothoracic "joint" kinematics
% -------------------------------------------------------------------------
% Initialisation
Joint(1).label    = 'Humerothoracic joint';
% Homogenous matrix of the rigid transformation between segments 1 and 2
Joint(1).T        = Mprod_array3(Tinv_array3(Bone(4).Trial.SCS.T),...
                                 Bone(1).Trial.SCS.T);
Joint(1).T        = Mprod_array3(Tinv_array3(Bone(4).Trial.SCS.T),...
                                 Bone(1).Trial.SCS.T);                             
% JCS and motion for the humerus relative to the thorax (Y�X�Y order) 
% (Wu et al. 2005)
Joint(1).sequence = 'YXY';               
Euler             = R2mobileYXY_array3(Joint(1).T(1:3,1:3,:));
Joint(1).PEL      = 180+unwrap(rad2deg(squeeze(Euler(:,1,:)))); % Plane of elevation, 0� is abduction, 90� is forward flexion
Joint(1).EL       = unwrap(rad2deg(squeeze(Euler(:,2,:)))); % Elevation (negative)
Joint(1).IER      = unwrap(rad2deg(squeeze(Euler(:,3,:)))); % Axial rotation, internal rotation (positive)
Joint(1).AP       = [];
Joint(1).ML       = [];
Joint(1).PD       = [];
clear Segment1 Segment2 Euler;          
% Plot
ijoint = 1;
p1     = figure(3);
sgtitle(p1,Joint(ijoint).label,'FontSize',14,'Fontweight','bold');
sp1 = subplot(2,3,1);
hold on; grid on; box on;
title(sp1,'Plane of elevation','FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Angle (�deg)','FontSize',12);
plot(Joint(ijoint).PEL);
sp1 = subplot(2,3,2);
hold on; grid on; box on;
title(sp1,'Elevation','FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Angle (�deg)','FontSize',12);
plot(Joint(ijoint).EL);
sp1 = subplot(2,3,3);
hold on; grid on; box on;
title(sp1,'Rotation','FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Angle (�deg)','FontSize',12);
plot(Joint(ijoint).IER);
 %%
% Glenohumeral joint kinematics
% -------------------------------------------------------------------------
% Initialisation
Joint(2).label    = 'Glenohumeral joint';
% Extract segments parameters
Segment1          = permute(Bone(1).Trial.SCS.Q,[2,3,1]);
Segment2          = permute(Bone(2).Trial.SCS.Q,[2,3,1]);
% Homogenous matrix of the rigid transformation between segments 1 and 2
Joint(2).T        = Mprod_array3(Tinv_array3(Q2Twu_array3(Segment2)),...
                                 Q2Tuv_array3(Segment1));
% JCS and motion for the humerus relative to the scapula (Y�X�Y order) 
% (Senk and Cheze 2006, Phadke et al. 2011)
Joint(2).sequence = 'XZY';               
Euler             = R2mobileXZY_array3(Joint(1).T(1:3,1:3,:));
dj                = Vnop_array3(Joint(2).T(1:3,4,:),... Di+1 to Pi in SCS of segment i+1
                                repmat([1;0;0],[1 1 n]),... % Xi+1 in SCS of segment i+1
                                Vnorm_array3(cross(repmat([1;0;0],[1 1 n]),Joint(2).T(1:3,2,:))),... Z = XxY
                                Joint(2).T(1:3,2,:)); % Yi in SCS of segment i
Joint(2).AA       = rad2deg(squeeze(Euler(:,1,:)));
Joint(2).FE       = rad2deg(squeeze(Euler(:,2,:)));
Joint(2).IER      = rad2deg(squeeze(Euler(:,3,:)));
Joint(2).AP       = squeeze(dj(1,:,:));
Joint(2).ML       = squeeze(dj(2,:,:));
Joint(2).PD       = squeeze(dj(3,:,:));
clear Segment1 Segment2 Euler dj;  
% Plot
ijoint = 2;
p1     = figure(4);
sgtitle(p1,Joint(ijoint).label,'FontSize',14,'Fontweight','bold');
sp1 = subplot(2,3,1);
hold on; grid on; box on;
title(sp1,'FE','FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Angle (�deg)','FontSize',12);
plot(Joint(ijoint).FE);
sp1 = subplot(2,3,2);
hold on; grid on; box on;
title(sp1,'AA','FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Angle (�deg)','FontSize',12);
plot(Joint(ijoint).AA);
sp1 = subplot(2,3,3);
hold on; grid on; box on;
title(sp1,'IER','FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Angle (�deg)','FontSize',12);
plot(Joint(ijoint).IER);
sp1 = subplot(2,3,4);
hold on; grid on; box on;
title(sp1,'AP','FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Displacement (m)','FontSize',12);
plot(Joint(ijoint).AP);
sp1 = subplot(2,3,5);
hold on; grid on; box on;
title(sp1,'ML','FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Displacement (m)','FontSize',12);
plot(Joint(ijoint).ML);
sp1 = subplot(2,3,6);
hold on; grid on; box on;
title(sp1,'PD','FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Displacement (m)','FontSize',12);
plot(Joint(ijoint).PD);
 %%
% Scapulothoracic joint kinematics
% -------------------------------------------------------------------------
% Initialisation
Joint(3).label    = 'Scapulothoracic joint';
% Extract segments parameters
Segment1          = permute(Bone(2).Trial.SCS.Q,[2,3,1]);
Segment2          = permute(Bone(4).Trial.SCS.Q,[2,3,1]);
% Homogenous matrix of the rigid transformation between segments 1 and 2
Joint(3).T        = Mprod_array3(Tinv_array3(Q2Twu_array3(Segment2)),...
                                 Q2Tuv_array3(Segment1));
% JCS and motion for the humerus relative to the scapula (Y�X�Y order) 
% (Senk and Cheze 2006, Phadke et al. 2011)
Joint(3).sequence = 'YXZ';  
Joint(3).legend   = {'Retraction (-) | Protraction (+)',...
                     'Lateral (-) | Medial (+) rotation',...
                     'Anterior (-) | Posterior (-) tilt',...
                     'X',...
                     'Y',...
                     'Z'};             
Euler             = R2mobileYXZ_array3(Joint(3).T(1:3,1:3,:));
dj                = Vnop_array3(Joint(3).T(1:3,4,:),... Di+1 to Pi in SCS of segment i+1
                                repmat([0;1;0],[1 1 n]),... % Yi+1 in SCS of segment i+1
                                Vnorm_array3(cross(repmat([0;1;0],[1 1 n]),Joint(3).T(1:3,3,:))),... X = YxZ
                                Joint(3).T(1:3,3,:)); % Zi in SCS of segment i
Joint(3).alpha    = rad2deg(squeeze(Euler(:,1,:)));
Joint(3).beta     = rad2deg(squeeze(Euler(:,2,:)));
Joint(3).gamma    = rad2deg(squeeze(Euler(:,3,:)));
Joint(3).dispX    = squeeze(dj(2,:,:));
Joint(3).dispY    = squeeze(dj(1,:,:));
Joint(3).dispZ    = squeeze(dj(3,:,:));
clear Segment1 Segment2 Euler dj;  
% Plot 
% Plot
ijoint = 3;
p1     = figure();
sgtitle(p1,Joint(ijoint).label,'FontSize',14,'Fontweight','bold');
%
sp1 = subplot(2,3,1);
hold on; grid on; box on;
title(sp1,Joint(3).legend{1},'FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Angle (�deg)','FontSize',12);
plot(-Joint(1).EL,Joint(ijoint).alpha);
%
sp1 = subplot(2,3,2);
hold on; grid on; box on;
title(sp1,Joint(3).legend{2},'FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Angle (�deg)','FontSize',12);
plot(-Joint(1).EL,Joint(ijoint).beta);
%
sp1 = subplot(2,3,3);
hold on; grid on; box on;
title(sp1,Joint(3).legend{3},'FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Angle (�deg)','FontSize',12);
plot(-Joint(1).EL,Joint(ijoint).gamma);
%
sp1 = subplot(2,3,4);
hold on; grid on; box on;
title(sp1,Joint(3).legend{4},'FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Displacement (m)','FontSize',12);
plot(-Joint(1).EL,Joint(ijoint).dispX);
%
sp1 = subplot(2,3,5);
hold on; grid on; box on;
title(sp1,Joint(3).legend{5},'FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Displacement (m)','FontSize',12);
plot(-Joint(1).EL,Joint(ijoint).dispY);
%
sp1 = subplot(2,3,6);
hold on; grid on; box on;
title(sp1,Joint(3).legend{6},'FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Displacement (m)','FontSize',12);
plot(-Joint(1).EL,Joint(ijoint).dispZ);
 %%
% Acromioclavicular joint kinematics
% -------------------------------------------------------------------------
% Initialisation
Joint(4).label    = 'Acromioclavicular joint';
% Extract segments parameters
Segment1          = permute(Bone(2).Trial.SCS.Q,[2,3,1]);
Segment2          = permute(Bone(3).Trial.SCS.Q,[2,3,1]);
% Homogenous matrix of the rigid transformation between segments 1 and 2
Joint(4).T        = Mprod_array3(Tinv_array3([permute(Bone(3).Trial.SCS.X,[2,3,1]) permute(Bone(3).Trial.SCS.Y,[2,3,1]) permute(Bone(3).Trial.SCS.Z,[2,3,1]) permute(Bone(3).Trial.SCS.O,[2,3,1]); zeros(1,1,n) zeros(1,1,n) zeros(1,1,n) ones(1,1,n)]),...
                                 Q2Tuv_array3(Segment1));
% JCS and motion for the scapula relative to the clavicle
% (Wu et al. 2005)
Joint(4).sequence = 'YXZ';  
Joint(4).legend   = {'Retraction (-) | Protraction (+)',...
                     'Lateral (-) | Medial (+) rotation',...
                     'Anterior (-) | Posterior (-) tilt',...
                     'X',...
                     'Y',...
                     'Z'};             
Euler             = R2mobileYXZ_array3(Joint(4).T(1:3,1:3,:));
dj                = Vnop_array3(Joint(4).T(1:3,4,:),... Di+1 to Pi in SCS of segment i+1
                                repmat([0;1;0],[1 1 n]),... % Yi+1 in SCS of segment i+1
                                Vnorm_array3(cross(repmat([0;1;0],[1 1 n]),Joint(4).T(1:3,3,:))),... X = YxZ
                                Joint(4).T(1:3,3,:)); % Zi in SCS of segment i
Joint(4).alpha    = rad2deg(squeeze(Euler(:,1,:)));
Joint(4).beta     = rad2deg(squeeze(Euler(:,2,:)));
Joint(4).gamma    = rad2deg(squeeze(Euler(:,3,:)));
Joint(4).dispX    = squeeze(dj(2,:,:));
Joint(4).dispY    = squeeze(dj(1,:,:));
Joint(4).dispZ    = squeeze(dj(3,:,:));
clear Segment1 Segment2 Euler dj;  
% Plot
ijoint = 4;
p1     = figure();
sgtitle(p1,Joint(ijoint).label,'FontSize',14,'Fontweight','bold');
%
sp1 = subplot(2,3,1);
hold on; grid on; box on;
title(sp1,Joint(5).legend{1},'FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Angle (�deg)','FontSize',12);
plot(-Joint(1).EL,Joint(ijoint).alpha);
%
sp1 = subplot(2,3,2);
hold on; grid on; box on;
title(sp1,Joint(5).legend{2},'FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Angle (�deg)','FontSize',12);
plot(-Joint(1).EL,Joint(ijoint).beta);
%
sp1 = subplot(2,3,3);
hold on; grid on; box on;
title(sp1,Joint(5).legend{3},'FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Angle (�deg)','FontSize',12);
plot(-Joint(1).EL,Joint(ijoint).gamma);
%
sp1 = subplot(2,3,4);
hold on; grid on; box on;
title(sp1,Joint(5).legend{4},'FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Displacement (m)','FontSize',12);
plot(-Joint(1).EL,Joint(ijoint).dispX);
%
sp1 = subplot(2,3,5);
hold on; grid on; box on;
title(sp1,Joint(5).legend{5},'FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Displacement (m)','FontSize',12);
plot(-Joint(1).EL,Joint(ijoint).dispY);
%
sp1 = subplot(2,3,6);
hold on; grid on; box on;
title(sp1,Joint(5).legend{6},'FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Displacement (m)','FontSize',12);
plot(-Joint(1).EL,Joint(ijoint).dispZ);
%%
% Sternoclavicular joint kinematics
% -------------------------------------------------------------------------
% Initialisation
Joint(5).label    = 'Sternoclavicular joint';
% Extract segments parameters
Segment1          = permute(Bone(3).Trial.SCS.Q,[2,3,1]);
Segment2          = permute(Bone(4).Trial.SCS.Q,[2,3,1]);
% Homogenous matrix of the rigid transformation between segments 1 and 2
Joint(5).T        = Mprod_array3(Tinv_array3(Q2Twu_array3(Segment2)),...
                                 [permute(Bone(3).Trial.SCS.X,[2,3,1]) permute(Bone(3).Trial.SCS.Y,[2,3,1]) permute(Bone(3).Trial.SCS.Z,[2,3,1]) permute(Bone(3).Trial.SCS.O,[2,3,1]); zeros(1,1,n) zeros(1,1,n) zeros(1,1,n) ones(1,1,n)]);%Q2Tuv_array3(Segment1));                             
% JCS and motion for the humerus relative to the scapula (Y�X�Y order) 
% (Senk and Cheze 2006, Phadke et al. 2011)
Joint(5).sequence = 'YXZ';
Joint(5).legend   = {'Retraction (-) | Protraction (+)',...
                     'Elevation (-) | Depression (+)',...
                     'Axial rotation top backwards (+) | forwards (-)',...
                     'X',...
                     'Y',...
                     'Z'};
Euler             = R2mobileYXZ_array3(Joint(5).T(1:3,1:3,:));
dj                = Vnop_array3(Joint(5).T(1:3,4,:),... Di+1 to Pi in SCS of segment i+1
                                repmat([0;1;0],[1 1 n]),... % Yi+1 in SCS of segment i+1
                                Vnorm_array3(cross(repmat([0;1;0],[1 1 n]),Joint(5).T(1:3,3,:))),... X = YxZ
                                Joint(5).T(1:3,3,:)); % Zi in SCS of segment i
Joint(5).alpha    = rad2deg(squeeze(Euler(:,3,:)));
Joint(5).beta     = rad2deg(squeeze(Euler(:,2,:)));
Joint(5).gamma    = rad2deg(squeeze(Euler(:,1,:)));
Joint(5).dispX    = squeeze(dj(2,:,:));
Joint(5).dispY    = squeeze(dj(1,:,:));
Joint(5).dispZ    = squeeze(dj(3,:,:));
clear Segment1 Segment2 Euler dj;  
% Plot
ijoint = 5;
p1     = figure();
sgtitle(p1,Joint(ijoint).label,'FontSize',14,'Fontweight','bold');
%
sp1 = subplot(2,3,1);
hold on; grid on; box on;
title(sp1,Joint(5).legend{1},'FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Angle (�deg)','FontSize',12);
plot(Joint(ijoint).alpha);
%
sp1 = subplot(2,3,2);
hold on; grid on; box on;
title(sp1,Joint(5).legend{2},'FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Angle (�deg)','FontSize',12);
plot(Joint(ijoint).beta);
%
sp1 = subplot(2,3,3);
hold on; grid on; box on;
title(sp1,Joint(5).legend{3},'FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Angle (�deg)','FontSize',12);
plot(Joint(ijoint).gamma);
%
sp1 = subplot(2,3,4);
hold on; grid on; box on;
title(sp1,Joint(5).legend{4},'FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Displacement (m)','FontSize',12);
plot(Joint(ijoint).dispX);
%
sp1 = subplot(2,3,5);
hold on; grid on; box on;
title(sp1,Joint(5).legend{5},'FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Displacement (m)','FontSize',12);
plot(Joint(ijoint).dispY);
%
sp1 = subplot(2,3,6);
hold on; grid on; box on;
title(sp1,Joint(5).legend{6},'FontSize',12);
xlabel(sp1,'Frames','FontSize',12);
ylabel(sp1,'Displacement (m)','FontSize',12);
plot(Joint(ijoint).dispZ);