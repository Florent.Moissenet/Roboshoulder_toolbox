% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/Roboshoulder_Toolbox
% Reference    : To be defined
% Date         : March 2020
% -------------------------------------------------------------------------
% Description  : To be defined
% -------------------------------------------------------------------------
% Dependencies : To be defined
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function Trial = InitialiseMarkerset(Trial,Marker,timeRange)

% Set markerset
markerSet = {'cTHO01','cTHO02','cTHO03','cTHO04',...
             'cRHUM01','cRHUM02','cRHUM03','cRHUM04',...
             'cRCLA01','cRCLA02','cRCLA03','cRCLA04',...
             'cRSCA01','cRSCA02','cRSCA03','cRSCA04',...
             'cLHUM01','cLHUM02','cLHUM03','cLHUM04',...
             'cLCLA01','cLCLA02','cLCLA03','cLCLA04',...
             'cLSCA01','cLSCA02','cLSCA03','cLSCA04',...
             'ROB_B01','ROB_B02','ROB_B03','ROB_B04',...
             'ROB_E01','ROB_E02','ROB_E03','ROB_E04'};      
         
% Set landmark type
% 'landmark' is a marker related to a rigid body
% 'semi-landmark' is a marker related to a curve
% 'hybrid-landmark' is a marker related to a curve and a rigid body
% 'technical' is a marker not used for anatomical description
landmarkList = {'technical','technical','technical','technical',...
                'technical','technical','technical','technical',...
                'technical','technical','technical','technical',...
                'technical','technical','technical','technical',...
                'technical','technical','technical','technical',...
                'technical','technical','technical','technical',...
                'technical','technical','technical','technical',...
                'landmark','landmark','landmark','landmark',...
                'landmark','landmark','landmark','landmark',};            
         
% Set related rigid segments
% Only used with landmark and hybrid-landmarks markers ('none' instead')
segmentList = {'Thorax','Thorax','Thorax','Thorax',...
               'RHumerus','RHumerus','RHumerus','RHumerus',...
               'RClavicle','RClavicle','RClavicle','RClavicle',...
               'RScapula','RScapula','RScapula','RScapula',...
               'LHumerus','LHumerus','LHumerus','LHumerus',...
               'LClavicle','LClavicle','LClavicle','LClavicle',...
               'LScapula','LScapula','LScapula','LScapula',...
               'Robot_base','Robot_base','Robot_base','Robot_base',...
               'RHumerus','RHumerus','RHumerus','RHumerus'}; % Rigid body assumed between humerus cluster and end effector};    
           
% Set related curves
% Only used with semi-landmark and hybrid-landmarks markers ('none' instead')
% Syntax: Curve named followed by order number on the curve
curveList = {'none',nan,'none',nan,'none',nan,'none',nan,...
             'none',nan,'none',nan,'none',nan,'none',nan,...
             'none',nan,'none',nan,'none',nan,'none',nan,...
             'none',nan,'none',nan,'none',nan,'none',nan,...
             'none',nan,'none',nan,'none',nan,'none',nan,...
             'none',nan,'none',nan,'none',nan,'none',nan,...
             'none',nan,'none',nan,'none',nan,'none',nan,...
             'none',nan,'none',nan,'none',nan,'none',nan,...
             'none',nan,'none',nan,'none',nan,'none',nan,...
             'none',nan,'none',nan}; 

% Initialise markers
Trial.Marker = [];
for i = 1:length(markerSet)
    Trial.Marker(i).label                 = markerSet{i};
    Trial.Marker(i).type                  = landmarkList{i};
    Trial.Marker(i).Body.Segment.label    = segmentList{i};
    Trial.Marker(i).Body.Curve.label      = curveList{i*2-1};
    Trial.Marker(i).Body.Curve.index      = curveList{i*2};
    Trial.Marker(i).Gap                   = [];
    if isfield(Marker,markerSet{i})
        Trial.Marker(i).Trajectory.raw    = permute(Marker.(markerSet{i})(timeRange(1):timeRange(2),:)*1e-3,[2,3,1]); % Convert mm to m
        Trial.Marker(i).Trajectory.fill   = [];
        Trial.Marker(i).Trajectory.smooth = [];
        Trial.Marker(i).Trajectory.cycle  = nan(3,1,101);
    else
        Trial.Marker(i).Trajectory.raw    = [];
        Trial.Marker(i).Trajectory.fill   = [];
        Trial.Marker(i).Trajectory.smooth = [];
        Trial.Marker(i).Trajectory.cycle  = nan(3,1,101);
    end
    Trial.Marker(i).smoothing             = 'none';
end