% -------------------------------------------------------------------------
% INIT THE WORKSPACE
% -------------------------------------------------------------------------
clearvars;
close all;
warning off;
clc;

% -------------------------------------------------------------------------
% SET FOLDERS
% -------------------------------------------------------------------------
Folder.toolbox = 'C:\Users\moissene\Documents\Switchdrive\Projets\B-LAB\Projets\2019-2020 - ROBOSHOULDER\Donnees\Roboshoulder_toolbox\';
Folder.data    = 'C:\Users\moissene\Documents\Switchdrive\Projets\B-LAB\Projets\2021-2022 - ROBOWRIST\ATHLANI_PROSUP\Data\RW005_R_bis\';
Folder.export  = 'C:\Users\moissene\Documents\Switchdrive\Projets\B-LAB\Projets\2021-2022 - ROBOWRIST\ATHLANI_PROSUP\Data\RW005_R_bis\';
Folder.dep     = [Folder.toolbox,'dependencies\']; % These binaries are prepared for a recent version of Matlab and Windows 8 or newer
addpath(Folder.toolbox);
addpath(genpath(Folder.dep));
cd(Folder.data);

% Get real marker positions stored in C3D files
% -------------------------------------------------------------------------
btkFile      = btkReadAcquisition('robotAxis6.c3d');
Marker       = btkGetMarkers(btkFile);
markerNames  = fieldnames(Marker); % Marker names in the marker trajectories
nMarker      = length(Marker.Link6_01); % n frames stored in the marker trajectories
fMarker      = btkGetPointFrequency(btkFile); % Marker trajectories frequency
dMarker      = 14; % Marker diameter (mm)

% O4 = Marker.Link4_01;
% X4 = Marker.Link4_05 - Marker.Link4_01;
% X4 = X4./sqrt(X4(:,1).^2+X4(:,2).^2+X4(:,3).^2);
% Y4 = Marker.Link4_03 - (Marker.Link4_02+Marker.Link4_04)/2;
% Y4 = Y4./sqrt(Y4(:,1).^2+Y4(:,2).^2+Y4(:,3).^2);
% Z4 = cross(X4,Y4);
% for t = 1:nMarker
%     T4(:,:,t) = [permute(X4(t,:),[2,3,1]) permute(Y4(t,:),[2,3,1]) permute(Z4(t,:),[2,3,1]) permute(O4(t,:),[2,3,1]); 0 0 0 1];
% end

O5 = Marker.Link5_01;
X5 = Marker.Link5_04 - Marker.Link5_01;
X5 = X5./sqrt(X5(:,1).^2+X5(:,2).^2+X5(:,3).^2);
Y5 = Marker.Link5_03 - Marker.Link5_05;
Y5 = Y5./sqrt(Y5(:,1).^2+Y5(:,2).^2+Y5(:,3).^2);
Z5 = cross(X5,Y5);
for t = 1:nMarker
    T5(:,:,t) = [permute(X5(t,:),[2,3,1]) permute(Y5(t,:),[2,3,1]) permute(Z5(t,:),[2,3,1]) permute(O5(t,:),[2,3,1]); 0 0 0 1];
end

O6 = Marker.Link6_01;
X6 = Marker.Link6_04 - Marker.Link6_01;
X6 = X6./sqrt(X6(:,1).^2+X6(:,2).^2+X6(:,3).^2);
Y6 = Marker.Link6_02 - Marker.Link6_05;
Y6 = Y6./sqrt(Y6(:,1).^2+Y6(:,2).^2+Y6(:,3).^2);
Z6 = cross(X6,Y6);
for t = 1:nMarker
    T6(:,:,t) = [permute(X6(t,:),[2,3,1]) permute(Y6(t,:),[2,3,1]) permute(Z6(t,:),[2,3,1]) permute(O6(t,:),[2,3,1]); 0 0 0 1];
end

[a,asi,asj,rA,rAsi,rAsj] = SARA_array3(T4,T5);

% [a,asi,asj,rA,rAsi,rAsj] = SARA_array3(T5,T6);

% Plot results    
% -------------------------------------------------------------------------     
figure;
for t = 1:10:nMarker
    plot3(Marker.Link5_01(t,1),Marker.Link5_01(t,2),Marker.Link5_01(t,3),'Marker','o','Color','blue');
    hold on;
    plot3(Marker.Link5_02(t,1),Marker.Link5_02(t,2),Marker.Link5_02(t,3),'Marker','o','Color','blue');
    plot3(Marker.Link5_03(t,1),Marker.Link5_03(t,2),Marker.Link5_03(t,3),'Marker','o','Color','blue');
    plot3(Marker.Link5_04(t,1),Marker.Link5_04(t,2),Marker.Link5_04(t,3),'Marker','o','Color','blue');
    plot3(Marker.Link5_05(t,1),Marker.Link5_05(t,2),Marker.Link5_05(t,3),'Marker','o','Color','blue');
%     plot3(Marker.Link4_01(t,1),Marker.Link4_01(t,2),Marker.Link4_01(t,3),'Marker','o','Color','green');
%     plot3(Marker.Link4_02(t,1),Marker.Link4_02(t,2),Marker.Link4_02(t,3),'Marker','o','Color','green');
%     plot3(Marker.Link4_03(t,1),Marker.Link4_03(t,2),Marker.Link4_03(t,3),'Marker','o','Color','green');
%     plot3(Marker.Link4_04(t,1),Marker.Link4_04(t,2),Marker.Link4_04(t,3),'Marker','o','Color','green');
%     plot3(Marker.Link4_05(t,1),Marker.Link4_05(t,2),Marker.Link4_05(t,3),'Marker','o','Color','green');
%     plot3(rA(1,1,t),rA(2,1,t),rA(3,1,t),'Marker','x','Color','black');
%     quiver3(rA(1,1,t),rA(2,1,t),rA(3,1,t),a(1,1,t),a(2,1,t),a(3,1,t),100,'Color','black');
    plot3(Marker.Link6_01(t,1),Marker.Link6_01(t,2),Marker.Link6_01(t,3),'Marker','o','Color','red');
    plot3(Marker.Link6_02(t,1),Marker.Link6_02(t,2),Marker.Link6_02(t,3),'Marker','o','Color','red');
    plot3(Marker.Link6_03(t,1),Marker.Link6_03(t,2),Marker.Link6_03(t,3),'Marker','o','Color','red');
    plot3(Marker.Link6_04(t,1),Marker.Link6_04(t,2),Marker.Link6_04(t,3),'Marker','o','Color','red');
    plot3(Marker.Link6_05(t,1),Marker.Link6_05(t,2),Marker.Link6_05(t,3),'Marker','o','Color','red');
    plot3(rA(1,1,t),rA(2,1,t),rA(3,1,t),'Marker','x','Color','black');
    quiver3(rA(1,1,t),rA(2,1,t),rA(3,1,t),a(1,1,t),a(2,1,t),a(3,1,t),100,'Color','black');
    axis equal;
    grid on;
    box on;
    xlabel('X axis');
    ylabel('Y axis');
    zlabel('Z axis');
    pause(0.1);
    hold off;
end