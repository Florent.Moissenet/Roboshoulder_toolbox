% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/Roboshoulder_Toolbox
% Reference    : To be defined
% Date         : March 2020
% -------------------------------------------------------------------------
% Description  : To be defined
% -------------------------------------------------------------------------
% Dependencies : To be defined
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------s

function [] = PlotRobotMotion(Trial)

% v = VideoWriter('test.avi');
% open(v); 
for i = 1:10:size(Trial.Robot.Segment(1).T.raw,1)
    for j = 1:size(Trial.Robot.Segment,2)
        plot3(Trial.Robot.Segment(j).T.raw(i,1,4), ...
              Trial.Robot.Segment(j).T.raw(i,2,4), ...
              Trial.Robot.Segment(j).T.raw(i,3,4), ...
              'Marker','.','MarkerSize',40,'Color',[255,145,6]/255);
        if j > 1
            line([Trial.Robot.Segment(j-1).T.raw(i,1,4) Trial.Robot.Segment(j).T.raw(i,1,4)], ...
                 [Trial.Robot.Segment(j-1).T.raw(i,2,4) Trial.Robot.Segment(j).T.raw(i,2,4)], ...
                 [Trial.Robot.Segment(j-1).T.raw(i,3,4) Trial.Robot.Segment(j).T.raw(i,3,4)], ...
                 'LineWidth',4,'Color',[255,145,6]/255);
        end
        hold on;
    end
    axis equal;
    grid on;
    box on;
    xlabel('X axis');
    ylabel('Y axis');
    zlabel('Z axis');
    xlim([-0.1 1.0]);
    ylim([-1.0 1.0]);
    zlim([0 1.0]);
%     view(0,0);
%     drawnow;
%     writeVideo(v,getframe(gcf));
    pause(0.01); 
    hold off;
end
% close(v);