% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/Roboshoulder_Toolbox
% Reference    : To be defined
% Date         : March 2020
% -------------------------------------------------------------------------
% Description  : This routine aims to compute the robot trajectory, i.e.  
%                the robot flange trajectory and quaternion
%                It is part of the toolkit grouping several methods used in 
%                the RoboShoulder project, a joined project with the HEPIA 
%                school at Geneva.
% -------------------------------------------------------------------------
% Dependencies : To be defined
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function ExportC3D(Trial,Specimen,Session,Folder)

% Set new C3D file
btkFile = btkNewAcquisition();
btkSetFrequency(btkFile,Trial.fmarker);
btkSetFrameNumber(btkFile,Trial.n1);
btkSetPointsUnit(btkFile,'marker','m');

% Append marker trajectories
for i = 1:size(Trial.Marker,2)
    if ~isempty(Trial.Marker(i).Trajectory.smooth)
        btkAppendPoint(btkFile,'marker',Trial.Marker(i).label,Trial.Marker(i).Trajectory.smooth);
    else
        btkAppendPoint(btkFile,'marker',Trial.Marker(i).label,zeros(Trial.n1,3));
    end
end

% Append specimen metadata
nData = 4;
info.format = 'Integer';
info.values = nData;
btkAppendMetaData(btkFile,'SPECIMEN','USED',info);
clear info;
info.format = 'Char';
info.dimensions = ['1x',nData];
info.values(1:nData) = {'gender' 'age' 'height' 'weight'};
btkAppendMetaData(btkFile,'SPECIMEN','LABELS',info);
clear info;
info.format = 'Char';
info.dimensions = ['1x',nData];
info.values(1:nData) = {'adimensioned (0: female, 1: male)' 'years' ...
                        'm' 'kg'};
btkAppendMetaData(btkFile,'SPECIMEN','UNITS',info);
clear info;
info.format     = 'Real';
info.dimensions = ['1x',nData];
if strcmp(Specimen.gender,'Male')
    info.values(2) = 1;
elseif strcmp(Specimen.gender,'Female')
    info.values(2) = 0;
end
info.values(3)  = Specimen.age;
info.values(4)  = Specimen.height*1e-2;
info.values(5)  = Specimen.weight;
btkAppendMetaData(btkFile,'SPECIMEN','VALUES',info);

% Append session metadata
nData                = 2;
info.format          = 'Integer';
info.values          = nData;
btkAppendMetaData(btkFile,'SESSION','USED',info);
clear info;
info.format          = 'Char';
info.dimensions      = ['1x',nData];
info.values(1:nData) = {'date' 'examiner'};
btkAppendMetaData(btkFile,'SESSION','LABELS',info);
clear info;
info.format          = 'Char';
info.dimensions      = ['1x',nData];
info.values(1:nData) = {'DD-MM-YYYY' 'initials'};
btkAppendMetaData(btkFile,'SESSION','UNITS',info);
clear info;
info.format          = 'Char';
info.dimensions      = ['1x',nData];
info.values(1:nData) = {Session.date Session.examiner};
btkAppendMetaData(btkFile,'SESSION','VALUES',info);

% Export C3D file
cd(Folder.export);
btkWriteAcquisition(btkFile,[regexprep(Trial.file,'.c3d',''),'_processed.c3d']);