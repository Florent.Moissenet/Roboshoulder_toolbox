% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/Roboshoulder_Toolbox
% Reference    : To be defined
% Date         : March 2020
% -------------------------------------------------------------------------
% Description  : To be defined
% -------------------------------------------------------------------------
% Dependencies : To be defined
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function Trial = GetRobotSegmentPlans(Folder,Trial)

% -------------------------------------------------------------------------
% SET MAT FILES RELATED TO ROBOT SEGMENT PLAN
% -------------------------------------------------------------------------
cd(Folder.data);
temp    = dir([Trial.motion,'_',Trial.side(1),'_full*.mat']);
matFile = '';
for i = 1:size(temp,1)    
    if ~contains(temp(i).name,'init')
        matFile = [matFile temp(i).name];
    end
end
Trial.Robot.fileMocap = matFile;
clear temp;

% -------------------------------------------------------------------------
% EXTRACT ROBOT SEGMENT PLAN FROM MAT FILES
% -------------------------------------------------------------------------
load(matFile);
Trial.Robot.Segment(9).Position.ref = permute(O_flange,[3,1,2]);
temp                                = permute(R_flange,[3,1,2]);
ICS                                 = [1 0 0; 0 1 0; 0 0 1];
for i = 1:size(temp,1)
    Trial.Robot.Segment(9).OrientationXY.ref(i,1) = rad2deg(atan2(norm(cross(temp(i,:,1),ICS(:,1))), dot(temp(i,:,1),ICS(:,1))));
    Trial.Robot.Segment(9).OrientationZ.ref(i,1)  = rad2deg(atan2(norm(cross(temp(i,:,3),ICS(:,3))), dot(temp(i,:,3),ICS(:,3))));
end