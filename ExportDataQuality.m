% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/Roboshoulder_Toolbox
% Reference    : To be defined
% Date         : March 2020
% -------------------------------------------------------------------------
% Description  : To be defined
% -------------------------------------------------------------------------
% Dependencies : To be defined
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function [] = ExportDataQuality(specimen,Quality)

% Joint / right
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.R.FE.normal.DTWr_mean,'right','C4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.R.FE.normal.DTWv_mean,'right','C5');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.R.FE.normal.DTWr_mean,'right','D4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.R.FE.normal.DTWv_mean,'right','D5');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.R.FE.normal.DTWr_mean,'right','E4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.R.FE.normal.DTWv_mean,'right','E5');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.R.FE.normal.DTWr_mean,'right','F4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.R.FE.normal.DTWv_mean,'right','F5');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.R.FE.normal.DTWr_mean,'right','G4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.R.FE.normal.DTWv_mean,'right','G5');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.R.FE.normal.DTWr_mean,'right','H4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.R.FE.normal.DTWv_mean,'right','H5');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.R.FE.normal.DTWr_mean,'right','I4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.R.FE.normal.DTWv_mean,'right','I5');

xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.R.AA.normal.DTWr_mean,'right','C6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.R.AA.normal.DTWv_mean,'right','C7');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.R.AA.normal.DTWr_mean,'right','D6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.R.AA.normal.DTWv_mean,'right','D7');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.R.AA.normal.DTWr_mean,'right','E6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.R.AA.normal.DTWv_mean,'right','E7');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.R.AA.normal.DTWr_mean,'right','F6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.R.AA.normal.DTWv_mean,'right','F7');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.R.AA.normal.DTWr_mean,'right','G6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.R.AA.normal.DTWv_mean,'right','G7');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.R.AA.normal.DTWr_mean,'right','H6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.R.AA.normal.DTWv_mean,'right','H7');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.R.AA.normal.DTWr_mean,'right','I6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.R.AA.normal.DTWv_mean,'right','I7');

xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.R.IER.normal.DTWr_mean,'right','C8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.R.IER.normal.DTWv_mean,'right','C9');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.R.IER.normal.DTWr_mean,'right','D8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.R.IER.normal.DTWv_mean,'right','D9');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.R.IER.normal.DTWr_mean,'right','E8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.R.IER.normal.DTWv_mean,'right','E9');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.R.IER.normal.DTWr_mean,'right','F8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.R.IER.normal.DTWv_mean,'right','F9');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.R.IER.normal.DTWr_mean,'right','G8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.R.IER.normal.DTWv_mean,'right','G9');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.R.IER.normal.DTWr_mean,'right','H8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.R.IER.normal.DTWv_mean,'right','H9');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.R.IER.normal.DTWr_mean,'right','I8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.R.IER.normal.DTWv_mean,'right','I9');

xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.R.HFE.normal.DTWr_mean,'right','C10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.R.HFE.normal.DTWv_mean,'right','C11');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.R.HFE.normal.DTWr_mean,'right','D10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.R.HFE.normal.DTWv_mean,'right','D11');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.R.HFE.normal.DTWr_mean,'right','E10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.R.HFE.normal.DTWv_mean,'right','E11');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.R.HFE.normal.DTWr_mean,'right','F10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.R.HFE.normal.DTWv_mean,'right','F11');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.R.HFE.normal.DTWr_mean,'right','G10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.R.HFE.normal.DTWv_mean,'right','G11');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.R.HFE.normal.DTWr_mean,'right','H10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.R.HFE.normal.DTWv_mean,'right','H11');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.R.HFE.normal.DTWr_mean,'right','I10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.R.HFE.normal.DTWv_mean,'right','I11');

xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.R.VT.normal.DTWr_mean,'right','C12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.R.VT.normal.DTWv_mean,'right','C13');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.R.VT.normal.DTWr_mean,'right','D12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.R.VT.normal.DTWv_mean,'right','D13');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.R.VT.normal.DTWr_mean,'right','E12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.R.VT.normal.DTWv_mean,'right','E13');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.R.VT.normal.DTWr_mean,'right','F12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.R.VT.normal.DTWv_mean,'right','F13');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.R.VT.normal.DTWr_mean,'right','G12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.R.VT.normal.DTWv_mean,'right','G13');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.R.VT.normal.DTWr_mean,'right','H12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.R.VT.normal.DTWv_mean,'right','H13');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.R.VT.normal.DTWr_mean,'right','I12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.R.VT.normal.DTWv_mean,'right','I13');

xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.R.HC.normal.DTWr_mean,'right','C14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.R.HC.normal.DTWv_mean,'right','C15');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.R.HC.normal.DTWr_mean,'right','D14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.R.HC.normal.DTWv_mean,'right','D15');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.R.HC.normal.DTWr_mean,'right','E14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.R.HC.normal.DTWv_mean,'right','E15');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.R.HC.normal.DTWr_mean,'right','F14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.R.HC.normal.DTWv_mean,'right','F15');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.R.HC.normal.DTWr_mean,'right','G14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.R.HC.normal.DTWv_mean,'right','G15');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.R.HC.normal.DTWr_mean,'right','H14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.R.HC.normal.DTWv_mean,'right','H15');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.R.HC.normal.DTWr_mean,'right','I14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.R.HC.normal.DTWv_mean,'right','I15');

% Joint / Left
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.L.FE.normal.DTWr_mean,'left','C4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.L.FE.normal.DTWv_mean,'left','C5');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.L.FE.normal.DTWr_mean,'left','D4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.L.FE.normal.DTWv_mean,'left','D5');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.L.FE.normal.DTWr_mean,'left','E4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.L.FE.normal.DTWv_mean,'left','E5');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.L.FE.normal.DTWr_mean,'left','F4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.L.FE.normal.DTWv_mean,'left','F5');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.L.FE.normal.DTWr_mean,'left','G4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.L.FE.normal.DTWv_mean,'left','G5');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.L.FE.normal.DTWr_mean,'left','H4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.L.FE.normal.DTWv_mean,'left','H5');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.L.FE.normal.DTWr_mean,'left','I4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.L.FE.normal.DTWv_mean,'left','I5');

xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.L.AA.normal.DTWr_mean,'left','C6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.L.AA.normal.DTWv_mean,'left','C7');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.L.AA.normal.DTWr_mean,'left','D6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.L.AA.normal.DTWv_mean,'left','D7');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.L.AA.normal.DTWr_mean,'left','E6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.L.AA.normal.DTWv_mean,'left','E7');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.L.AA.normal.DTWr_mean,'left','F6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.L.AA.normal.DTWv_mean,'left','F7');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.L.AA.normal.DTWr_mean,'left','G6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.L.AA.normal.DTWv_mean,'left','G7');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.L.AA.normal.DTWr_mean,'left','H6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.L.AA.normal.DTWv_mean,'left','H7');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.L.AA.normal.DTWr_mean,'left','I6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.L.AA.normal.DTWv_mean,'left','I7');

xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.L.IER.normal.DTWr_mean,'left','C8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.L.IER.normal.DTWv_mean,'left','C9');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.L.IER.normal.DTWr_mean,'left','D8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.L.IER.normal.DTWv_mean,'left','D9');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.L.IER.normal.DTWr_mean,'left','E8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.L.IER.normal.DTWv_mean,'left','E9');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.L.IER.normal.DTWr_mean,'left','F8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.L.IER.normal.DTWv_mean,'left','F9');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.L.IER.normal.DTWr_mean,'left','G8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.L.IER.normal.DTWv_mean,'left','G9');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.L.IER.normal.DTWr_mean,'left','H8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.L.IER.normal.DTWv_mean,'left','H9');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.L.IER.normal.DTWr_mean,'left','I8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.L.IER.normal.DTWv_mean,'left','I9');

xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.L.HFE.normal.DTWr_mean,'left','C10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.L.HFE.normal.DTWv_mean,'left','C11');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.L.HFE.normal.DTWr_mean,'left','D10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.L.HFE.normal.DTWv_mean,'left','D11');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.L.HFE.normal.DTWr_mean,'left','E10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.L.HFE.normal.DTWv_mean,'left','E11');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.L.HFE.normal.DTWr_mean,'left','F10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.L.HFE.normal.DTWv_mean,'left','F11');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.L.HFE.normal.DTWr_mean,'left','G10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.L.HFE.normal.DTWv_mean,'left','G11');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.L.HFE.normal.DTWr_mean,'left','H10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.L.HFE.normal.DTWv_mean,'left','H11');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.L.HFE.normal.DTWr_mean,'left','I10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.L.HFE.normal.DTWv_mean,'left','I11');

xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.L.VT.normal.DTWr_mean,'left','C12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.L.VT.normal.DTWv_mean,'left','C13');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.L.VT.normal.DTWr_mean,'left','D12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.L.VT.normal.DTWv_mean,'left','D13');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.L.VT.normal.DTWr_mean,'left','E12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.L.VT.normal.DTWv_mean,'left','E13');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.L.VT.normal.DTWr_mean,'left','F12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.L.VT.normal.DTWv_mean,'left','F13');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.L.VT.normal.DTWr_mean,'left','G12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.L.VT.normal.DTWv_mean,'left','G13');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.L.VT.normal.DTWr_mean,'left','H12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.L.VT.normal.DTWv_mean,'left','H13');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.L.VT.normal.DTWr_mean,'left','I12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.L.VT.normal.DTWv_mean,'left','I13');

xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.L.HC.normal.DTWr_mean,'left','C14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(1).Angle.L.HC.normal.DTWv_mean,'left','C15');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.L.HC.normal.DTWr_mean,'left','D14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(2).Angle.L.HC.normal.DTWv_mean,'left','D15');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.L.HC.normal.DTWr_mean,'left','E14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(3).Angle.L.HC.normal.DTWv_mean,'left','E15');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.L.HC.normal.DTWr_mean,'left','F14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(4).Angle.L.HC.normal.DTWv_mean,'left','F15');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.L.HC.normal.DTWr_mean,'left','G14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(5).Angle.L.HC.normal.DTWv_mean,'left','G15');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.L.HC.normal.DTWr_mean,'left','H14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(6).Angle.L.HC.normal.DTWv_mean,'left','H15');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.L.HC.normal.DTWr_mean,'left','I14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Joint(7).Angle.L.HC.normal.DTWv_mean,'left','I15');

% End effector / Right
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).R.FE.normal.DTWr_mean,'right','M4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).R.FE.normal.DTWv_mean,'right','M5');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).R.FE.normal.DTWr_mean,'right','N4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).R.FE.normal.DTWv_mean,'right','N5');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).R.FE.normal.DTWr_mean,'right','O4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).R.FE.normal.DTWv_mean,'right','O5');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.R.FE.normal.DTWr_mean,'right','P4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.R.FE.normal.DTWv_mean,'right','P5');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.R.FE.normal.DTWr_mean,'right','Q4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.R.FE.normal.DTWv_mean,'right','Q5');

xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).R.AA.normal.DTWr_mean,'right','M6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).R.AA.normal.DTWv_mean,'right','M7');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).R.AA.normal.DTWr_mean,'right','N6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).R.AA.normal.DTWv_mean,'right','N7');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).R.AA.normal.DTWr_mean,'right','O6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).R.AA.normal.DTWv_mean,'right','O7');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.R.AA.normal.DTWr_mean,'right','P6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.R.AA.normal.DTWv_mean,'right','P7');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.R.AA.normal.DTWr_mean,'right','Q6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.R.AA.normal.DTWv_mean,'right','Q7');

xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).R.IER.normal.DTWr_mean,'right','M8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).R.IER.normal.DTWv_mean,'right','M9');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).R.IER.normal.DTWr_mean,'right','N8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).R.IER.normal.DTWv_mean,'right','N9');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).R.IER.normal.DTWr_mean,'right','O8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).R.IER.normal.DTWv_mean,'right','O9');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.R.IER.normal.DTWr_mean,'right','P8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.R.IER.normal.DTWv_mean,'right','P9');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.R.IER.normal.DTWr_mean,'right','Q8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.R.IER.normal.DTWv_mean,'right','Q9');

xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).R.HFE.normal.DTWr_mean,'right','M10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).R.HFE.normal.DTWv_mean,'right','M11');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).R.HFE.normal.DTWr_mean,'right','N10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).R.HFE.normal.DTWv_mean,'right','N11');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).R.HFE.normal.DTWr_mean,'right','O10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).R.HFE.normal.DTWv_mean,'right','O11');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.R.HFE.normal.DTWr_mean,'right','P10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.R.HFE.normal.DTWv_mean,'right','P11');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.R.HFE.normal.DTWr_mean,'right','Q10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.R.HFE.normal.DTWv_mean,'right','Q11');

xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).R.VT.normal.DTWr_mean,'right','M12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).R.VT.normal.DTWv_mean,'right','M13');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).R.VT.normal.DTWr_mean,'right','N12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).R.VT.normal.DTWv_mean,'right','N13');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).R.VT.normal.DTWr_mean,'right','O12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).R.VT.normal.DTWv_mean,'right','O13');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.R.VT.normal.DTWr_mean,'right','P12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.R.VT.normal.DTWv_mean,'right','P13');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.R.VT.normal.DTWr_mean,'right','Q12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.R.VT.normal.DTWv_mean,'right','Q13');

xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).R.HC.normal.DTWr_mean,'right','M14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).R.HC.normal.DTWv_mean,'right','M15');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).R.HC.normal.DTWr_mean,'right','N14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).R.HC.normal.DTWv_mean,'right','N15');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).R.HC.normal.DTWr_mean,'right','O14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).R.HC.normal.DTWv_mean,'right','O15');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.R.HC.normal.DTWr_mean,'right','P14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.R.HC.normal.DTWv_mean,'right','P15');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.R.HC.normal.DTWr_mean,'right','Q14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.R.HC.normal.DTWv_mean,'right','Q15');

% End effector / Left
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).L.FE.normal.DTWr_mean,'left','M4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).L.FE.normal.DTWv_mean,'left','M5');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).L.FE.normal.DTWr_mean,'left','N4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).L.FE.normal.DTWv_mean,'left','N5');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).L.FE.normal.DTWr_mean,'left','O4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).L.FE.normal.DTWv_mean,'left','O5');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.L.FE.normal.DTWr_mean,'left','P4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.L.FE.normal.DTWv_mean,'left','P5');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.L.FE.normal.DTWr_mean,'left','Q4');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.L.FE.normal.DTWv_mean,'left','Q5');

xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).L.AA.normal.DTWr_mean,'left','M6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).L.AA.normal.DTWv_mean,'left','M7');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).L.AA.normal.DTWr_mean,'left','N6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).L.AA.normal.DTWv_mean,'left','N7');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).L.AA.normal.DTWr_mean,'left','O6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).L.AA.normal.DTWv_mean,'left','O7');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.L.AA.normal.DTWr_mean,'left','P6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.L.AA.normal.DTWv_mean,'left','P7');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.L.AA.normal.DTWr_mean,'left','Q6');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.L.AA.normal.DTWv_mean,'left','Q7');

xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).L.IER.normal.DTWr_mean,'left','M8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).L.IER.normal.DTWv_mean,'left','M9');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).L.IER.normal.DTWr_mean,'left','N8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).L.IER.normal.DTWv_mean,'left','N9');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).L.IER.normal.DTWr_mean,'left','O8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).L.IER.normal.DTWv_mean,'left','O9');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.L.IER.normal.DTWr_mean,'left','P8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.L.IER.normal.DTWv_mean,'left','P9');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.L.IER.normal.DTWr_mean,'left','Q8');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.L.IER.normal.DTWv_mean,'left','Q9');

xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).L.HFE.normal.DTWr_mean,'left','M10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).L.HFE.normal.DTWv_mean,'left','M11');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).L.HFE.normal.DTWr_mean,'left','N10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).L.HFE.normal.DTWv_mean,'left','N11');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).L.HFE.normal.DTWr_mean,'left','O10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).L.HFE.normal.DTWv_mean,'left','O11');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.L.HFE.normal.DTWr_mean,'left','P10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.L.HFE.normal.DTWv_mean,'left','P11');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.L.HFE.normal.DTWr_mean,'left','Q10');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.L.HFE.normal.DTWv_mean,'left','Q11');

xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).L.VT.normal.DTWr_mean,'left','M12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).L.VT.normal.DTWv_mean,'left','M13');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).L.VT.normal.DTWr_mean,'left','N12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).L.VT.normal.DTWv_mean,'left','N13');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).L.VT.normal.DTWr_mean,'left','O12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).L.VT.normal.DTWv_mean,'left','O13');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.L.VT.normal.DTWr_mean,'left','P12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.L.VT.normal.DTWv_mean,'left','P13');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.L.VT.normal.DTWr_mean,'left','Q12');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.L.VT.normal.DTWv_mean,'left','Q13');

xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).L.HC.normal.DTWr_mean,'left','M14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(1).L.HC.normal.DTWv_mean,'left','M15');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).L.HC.normal.DTWr_mean,'left','N14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(2).L.HC.normal.DTWv_mean,'left','N15');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).L.HC.normal.DTWr_mean,'left','O14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).Position(3).L.HC.normal.DTWv_mean,'left','O15');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.L.HC.normal.DTWr_mean,'left','P14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationXY.L.HC.normal.DTWv_mean,'left','P15');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.L.HC.normal.DTWr_mean,'left','Q14');
xlswrite([specimen,'_Quality.xlsx'],Quality.Robot.Segment(9).OrientationZ.L.HC.normal.DTWv_mean,'left','Q15');