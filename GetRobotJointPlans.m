% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/Roboshoulder_Toolbox
% Reference    : To be defined
% Date         : March 2020
% -------------------------------------------------------------------------
% Description  : To be defined
% -------------------------------------------------------------------------
% Dependencies : To be defined
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function Trial = GetRobotJointPlans(Folder,Trial)

% -------------------------------------------------------------------------
% SET YAML FILES RELATED TO ROBOT JOINT PLAN
% -------------------------------------------------------------------------
cd(Folder.data);
temp     = dir(['*_',Trial.motion,'_',Trial.side(1),'*.yaml']);
yamlFile = '';
for i = 1:size(temp,1)    
    if ~contains(temp(i).name,'init')
        if ~contains(Trial.direction,'reversed') && ~contains(temp(i).name,'reversed')
            yamlFile = [yamlFile temp(i).name];
        elseif contains(Trial.direction,'reversed') && contains(temp(i).name,'reversed')
            yamlFile = [yamlFile temp(i).name];
        end 
    end
end
Trial.Robot.filePlan = yamlFile;
clear temp;

% -------------------------------------------------------------------------
% EXTRACT ROBOT JOINT PLAN FROM YAML FILES
% -------------------------------------------------------------------------

% Get number of lines in the file
fid  = fopen(yamlFile,'r');
n = 0;
tline = fgetl(fid);
while ischar(tline)
  tline = fgetl(fid);
  n = n+1;
end
nlines = n;
fclose(fid);

% Extract joint plan data
fid  = fopen(yamlFile,'r');
for i = 1:22
    fgetl(fid);
end
test(:,1) = fscanf(fid,'        - %f');
ilines = 1;
k = 2;
while ilines <= nlines-35
    for i = 1:24
        fgetl(fid);
        ilines = ilines+1;
    end
    test(:,k) = fscanf(fid,'        - %f');
    k = k+1;
    ilines = ilines+8;
end
fclose(fid);

% Store reference data
for i = 1:size(test,1) %7
    Trial.Robot.Joint(i).Angle.ref = test(i,:)';
end
Trial.Robot.Joint(8).Angle.ref = []; % Joint between Link 7 and Flange
clear yamlFile;