% Author       : F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License      : Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code  : https://github.com/fmoissenet/Roboshoulder_Toolbox
% Reference    : To be defined
% Date         : March 2020
% -------------------------------------------------------------------------
% Description  : To be defined
% -------------------------------------------------------------------------
% Dependencies : To be defined
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

function Trial = CropMarkerTrajectories(Trial,Event,motionList,motionNumber)

% -------------------------------------------------------------------------
% IDENTIFY CYCLE EVENTS
% -------------------------------------------------------------------------

% Set the trajectory used for the following process
if strcmp(Trial.side,'Right')
    X = Trial.Marker(13).Trajectory.smooth(:,1);
elseif strcmp(Trial.side,'Left')
    X = Trial.Marker(26).Trajectory.smooth(:,1);
end

% Find true events based on estimated events defined in original C3D file
k = 1;
for i = 1:size(Event.(motionList{motionNumber}),2)-1
    
    % Identify the beginning of the first part of the cycle
    start = Event.(motionList{motionNumber})(i)*Trial.fmarker-Trial.n0;
    stop  = Event.(motionList{motionNumber})(i+1)*Trial.fmarker-Trial.n0;
    for j = fix(start):1:fix(start)+200
        threshold = 5e-5;
        if abs(X(j)-mean(X(fix(start):1:fix(start)+10))) > threshold
            trueStart1 = j;
            break;
        end
    end
    Cycle(k).label = [motionList{motionNumber}];
    Cycle(k).start = trueStart1;
    
    % Identify the end of the first part of the cycle
    for j = fix((start+stop)/2):-1:fix((start+stop)/2)-200
        threshold = 5e-5;
        if abs(X(j)-mean(X((start+stop)/2:-1:(start+stop)/2-10))) > threshold
            trueStop1 = j;
            break;
        end
    end
    Cycle(k).stop = trueStop1;
    k             = k+1;
    
    % Identify the beginning of the second part of the cycle
    for j = fix((start+stop)/2):1:fix((start+stop)/2)+200
        threshold = 5e-5;
        if abs(X(j)-mean(X(fix((start+stop)/2):1:fix((start+stop)/2)+10))) > threshold
            trueStart2 = j;
            break;
        end
    end    
    Cycle(k).label = [motionList{motionNumber},'_return'];
    Cycle(k).start = trueStart2;
    
    % Identify the end of the second part of the cycle
    for j = fix(stop):-1:fix(stop)-200
        threshold = 5e-5;
        if abs(X(j)-mean(X(stop:-1:stop-10))) > threshold
            trueStop2 = j;
            break;
        end
    end
    Cycle(k).stop = trueStop2;
    k             = k+1;
    
    clear start stop trueStart1 trueStop1 trueStart2 trueStop2;
end

% -------------------------------------------------------------------------
% CROP MARKER TRAJECTORIES 
% -------------------------------------------------------------------------
for i = 1:size(Trial.Marker,2)
    for j = 1:size(Cycle,2)
        n  = Cycle(j).stop-Cycle(j).start+1;
        k  = (1:n)';
        k0 = (linspace(1,n,101))';
        Trial.Marker(i).Trajectory.cycle(:,1,j) = interp1(k,...
                                                          Trial.Marker(i).Trajectory.smooth(Cycle(j).start:Cycle(j).stop,1),...
                                                          k0,'spline');
        Trial.Marker(i).Trajectory.cycle(:,2,j) = interp1(k,...
                                                          Trial.Marker(i).Trajectory.smooth(Cycle(j).start:Cycle(j).stop,2),...
                                                          k0,'spline');
        Trial.Marker(i).Trajectory.cycle(:,3,j) = interp1(k,...
                                                          Trial.Marker(i).Trajectory.smooth(Cycle(j).start:Cycle(j).stop,3),...
                                                          k0,'spline');
        clear n n0;
    end
end